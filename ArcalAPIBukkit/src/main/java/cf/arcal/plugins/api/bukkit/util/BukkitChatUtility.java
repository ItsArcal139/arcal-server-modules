package cf.arcal.plugins.api.bukkit.util;

import net.md_5.bungee.api.chat.BaseComponent;
import org.bukkit.*;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.*;
import cf.arcal.plugins.api.util.*;

public class BukkitChatUtility extends ChatUtility {
    private BaseComponent processComponent(JSONComponent component) {
        JSONComponent result = this.getPrefix().clone().addExtra(component);
        return BukkitJSONTextBuilder.build(result)[0];
    }

    public void sendToSender(CommandSender sender, JSONComponent component) {
        sender.spigot().sendMessage(this.processComponent(component));
    }

    @Override
    public void sendToPlayer(PlayerProvider provider, JSONComponent component) {
        ((Player) provider.getPlayer()).spigot().sendMessage(this.processComponent(component));
    }

    public void sendToPlayer(Player p, JSONComponent component) {
        this.sendToPlayer(new BukkitPlayerProvider(p), component);
    }

    @Override
    public void broadcast(JSONComponent component) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            this.sendToPlayer(p, component);
        }
    }

    @Override
    public void log(JSONComponent component) {
        Bukkit.getConsoleSender().spigot().sendMessage(this.processComponent(component));
    }
}
