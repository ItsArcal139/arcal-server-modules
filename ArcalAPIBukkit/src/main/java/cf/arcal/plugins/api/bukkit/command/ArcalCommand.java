package cf.arcal.plugins.api.bukkit.command;

import java.util.*;

import org.bukkit.command.*;

public abstract class ArcalCommand extends Command {
    protected ArcalCommand(String name) {
        super(name);
    }

    protected ArcalCommand(String name, String permission, String... aliases) {
        this(name);
        this.setPermission(permission);
        this.setAliases(Arrays.asList(aliases));
    }

    public abstract void execute(CommandSender sender, String[] args);
}
