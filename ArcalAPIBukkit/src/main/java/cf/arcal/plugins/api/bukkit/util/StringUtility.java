package cf.arcal.plugins.api.bukkit.util;

import org.bukkit.*;

public class StringUtility {
    public static String locToString(Location loc) {
        return "(" + loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ() +
            ")@" + loc.getWorld().getName();
    }

    public static String joinArray(String[] arr, String delimiter) {
        StringBuilder result = new StringBuilder();
        for (String item : arr) {
            result.append(delimiter).append(item);
        }
        return result.substring(delimiter.length());
    }
}
