package cf.arcal.plugins.api.bukkit.util;

import org.bukkit.*;

import java.util.*;

import cf.arcal.plugins.api.exceptions.*;
import io.puharesource.mc.titlemanager.api.v2.*;
import org.bukkit.entity.*;

public class TitleManagerBannerService extends BannerServiceBase {
    private TitleManagerAPI api;

    public TitleManagerBannerService() {
        if (Bukkit.getPluginManager().getPlugin("TitleManager") == null) {
            throw new ArcalAPIException("TitleManager is not installed.");
        }
        this.api = (TitleManagerAPI) Bukkit.getServer().getPluginManager().getPlugin("TitleManager");
    }

    public TitleManagerAPI getAPI() {
        return this.api;
    }

    @Override
    public void setLinesForPlayer(Player p, List<String> lines) {
        // TitleManager would start writing at 15, and ends at 1.
        int index = 15;
        for (int i = 0; i < lines.size() && index >= 1; i++, index--) {
            api.setScoreboardValue(p, index, lines.get(i));
        }
        for (; index >= 1; index--) {
            api.setScoreboardValue(p, index, null);
        }
    }
}
