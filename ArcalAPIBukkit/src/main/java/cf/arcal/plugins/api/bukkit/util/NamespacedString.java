package cf.arcal.plugins.api.bukkit.util;

public class NamespacedString {
    public static String prettify(String str) {
        String[] path = str.split("/");
        StringBuilder sb = new StringBuilder();
        for (String pathname : path) {
            sb.append("-> ");
            String[] words = pathname.split("_");
            for (String word : words) {
                sb.append(word.substring(0, 1).toUpperCase());
                sb.append(word.substring(1));
                sb.append(" ");
            }
        }

        String result = sb.toString();
        result = result.substring(3).trim();
        return result;
    }
}
