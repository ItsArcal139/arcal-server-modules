package cf.arcal.plugins.api.bukkit.command;

import cf.arcal.plugins.api.bukkit.*;
import cf.arcal.plugins.api.util.JSONColor;
import cf.arcal.plugins.api.util.JSONTextBuilder;
import org.bukkit.command.*;

public class CommandAapi extends ArcalCommand {
    private static CommandAapi instance = null;

    public static CommandAapi getInstance() {
        if (instance == null) instance = new CommandAapi();
        return instance;
    }

    private CommandAapi() {
        super("aapi", "arcalapi.cmduse.aapi", "arcalapi");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            this.showHelp(sender, args);
            return;
        }

        String subcmd = args[0];
        switch (subcmd) {
            case "reload":
                this.doReload(sender);
                break;
            default:
                this.doUnknownAction(sender, args);
                this.showHelp(sender, args);
                break;
        }
    }

    private void doReload(CommandSender sender) {
        ArcalAPIBukkit api = ArcalAPIBukkit.getInstance();
        if (sender.hasPermission(this.getPermission() + ".reload")) {
            api.chat().sendToSender(sender, JSONTextBuilder.text("Reloading configs...").setColor(JSONColor.GREEN));
            api.reload();
        } else {
            api.chat().sendToSender(sender, JSONTextBuilder.text("You don't have permission to do this.").setColor(JSONColor.RED));
        }
    }

    private void doUnknownAction(CommandSender sender, String[] args) {
        ArcalAPIBukkit api = ArcalAPIBukkit.getInstance();
        api.chat().sendToSender(sender,
            JSONTextBuilder.translatable("Unknown subcommand %s.")
                .addWith(JSONTextBuilder.text("\"" + args[0] + "\"").setColor(JSONColor.WHITE))
                .setColor(JSONColor.RED)
        );
    }

    private void showHelp(CommandSender sender, String[] args) {
        ArcalAPIBukkit api = ArcalAPIBukkit.getInstance();
        api.chat().sendToSender(sender, JSONTextBuilder.text("Usage:"));

        String format = "  %s - %s";
        if (sender.hasPermission("arcalapi.cmdsee.aapi.reload")) {
            api.chat().sendToSender(sender,
                JSONTextBuilder.translatable(format)
                    .addWith(JSONTextBuilder.text("/" + this.getName() + " reload").setColor(JSONColor.GOLD))
                    .addWith(JSONTextBuilder.text("Reloads the configuration."))
                    .alternativeColorCode('&').setColor(JSONColor.WHITE)
            );
        }
    }

    @Override
    public boolean execute(CommandSender cs, String string, String[] strings) {
        this.execute(cs, strings);
        return true;
    }
}
