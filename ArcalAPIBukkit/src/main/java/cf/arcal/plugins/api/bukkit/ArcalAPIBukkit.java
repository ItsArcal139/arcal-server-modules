/*
 * @author Arcal
 */
package cf.arcal.plugins.api.bukkit;

import cf.arcal.plugins.api.ArcalAPIPlatform;

import java.io.IOException;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import cf.arcal.plugins.api.bukkit.command.ArcalCommand;
import cf.arcal.plugins.api.bukkit.command.CommandAapi;
import cf.arcal.plugins.api.bukkit.command.CommandSkull;
import cf.arcal.plugins.api.bukkit.config.ArcalAPIBukkitConfig;
import cf.arcal.plugins.api.bukkit.config.BukkitConfigProvider;
import cf.arcal.plugins.api.bukkit.util.BukkitChatUtility;
import cf.arcal.plugins.api.bukkit.util.BukkitJSONTextBuilder;
import cf.arcal.plugins.api.config.ConfigProvider;
import cf.arcal.plugins.api.util.JSONColor;
import cf.arcal.plugins.api.util.JSONComponent;
import cf.arcal.plugins.api.util.JSONTextBuilder;
import cf.arcal.plugins.api.util.JSONTextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemMergeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.plugin.java.JavaPlugin;

public class ArcalAPIBukkit extends JavaPlugin implements Listener, ArcalAPIPlatform {
    private BukkitChatUtility chatUtil;
    private static ArcalAPIBukkit instance = null;
    private ArcalAPIBukkitConfig config = null;

    @Override
    public void onEnable() {
        instance = this;
        this.registerCommand(this, CommandAapi.getInstance());
        this.registerCommand(this, CommandSkull.getInstance());
        Bukkit.getPluginManager().registerEvents(this, this);

        this.chatUtil = new BukkitChatUtility();
        config = ArcalAPIBukkitConfig.getInstance();
    }

    public boolean applyPlayerMoney(Player p, double amount) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, IllegalStateException {
        // Our reflection part!
        Class ecoClazz = Class.forName("net.milkbowl.vault.economy.Economy");
        RegisteredServiceProvider service = Bukkit.getServicesManager().getRegistration(ecoClazz);
        if (service == null) {
            throw new IllegalStateException("Vault Economy not registered.");
        }

        Object ecoProvider = service.getProvider();
        if (ecoProvider == null) {
            throw new IllegalStateException("No Vault Economy provider.");
        }

        Class providerClazz = ecoProvider.getClass();
        Method depositPlayer = providerClazz.getDeclaredMethod(amount >= 0 ? "depositPlayer" : "withdrawPlayer", String.class, Double.TYPE);
        Object ecoResponse = depositPlayer.invoke(ecoProvider, p.getName(), Math.abs(amount));

        if (ecoResponse == null) {
            throw new IllegalStateException("No ecoResponse (?");
        }

        Class ecoRespClazz = Class.forName("net.milkbowl.vault.economy.EconomyResponse");
        Method transactionSuccess = ecoRespClazz.getDeclaredMethod("transactionSuccess");
        return (boolean) transactionSuccess.invoke(ecoResponse);
    }

    @Override
    public BukkitChatUtility chat() {
        return this.chatUtil;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ArcalAPIBukkitConfig config() {
        return this.config;
    }

    public static ArcalAPIBukkit getInstance() {
        return instance;
    }

    public JSONTextComponent getPlayerComponent(Player p) {
        return (JSONTextComponent) new JSONTextComponent(p.getPlayerListName())
            .setHoverEvent(JSONComponent.HoverEvent.showEntity(p.getPlayerListName(), "minecraft:player", p.getUniqueId()))
            .setClickEvent(JSONComponent.ClickEvent.suggestCommand("/tell " + p.getName() + " "));
    }

    public JSONTextComponent getPlayerComponent(OfflinePlayer p) {
        return (JSONTextComponent) new JSONTextComponent(p.getName())
            .setHoverEvent(JSONComponent.HoverEvent.showEntity(p.getName(), "minecraft:player", p.getUniqueId()))
            .setClickEvent(JSONComponent.ClickEvent.suggestCommand("/tell " + p.getName() + " "));
    }

    public JSONComponent getItemComponent(ItemStack item, String name, String format) {
        String a = getServer().getClass().getPackage().getName();
        String version = a.substring(a.lastIndexOf('.') + 1);

        try {
            Class craftItemStackClz = Class.forName("org.bukkit.craftbukkit." + version + ".inventory.CraftItemStack");
            Method asNMSCopyMethod = craftItemStackClz.getMethod("asNMSCopy", ItemStack.class);
            Object nmsItem = asNMSCopyMethod.invoke(null, item);

            Class nmsItemClz = nmsItem.getClass();
            Method getTagMethod = nmsItemClz.getDeclaredMethod("getTag");
            Object tag = getTagMethod.invoke(nmsItem);

            String itemId = "minecraft:";
            String itemStr = nmsItem.toString();
            String itemStrPrefix = "item.minecraft.";
            itemId += itemStr.substring(itemStrPrefix.length() + itemStr.indexOf(itemStrPrefix));

            String tagStr = "{id:\"" + itemId + "\",Count:" + item.getAmount() +",tag:" + tag.toString() + "}";
            this.chat().log(JSONTextBuilder.text(tagStr));

            return JSONTextBuilder.translatable(format)
                .addWith(JSONTextBuilder.text(name))
                .setHoverEvent(JSONComponent.HoverEvent.showItem(tagStr));
        } catch(ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
            this.chat().log(JSONTextBuilder.text("Failed to access NMS code!").setColor(JSONColor.RED));
            ex.printStackTrace();
            return JSONTextBuilder.translatable(format)
                .addWith(JSONTextBuilder.text(name));
        }
    }

    @Override
    public void onDisable() {
        instance = null;
    }

    /**
     * Called when items are being merged.
     *
     * @param e The {@code ItemMergeEvent}.
     */
    @EventHandler
    public void onItemMerge(ItemMergeEvent e) {
        e.setCancelled(config.doesCancelItemMerging());
    }

    @EventHandler
    public void onPlayerLogin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        Location loc = p.getWorld().getSpawnLocation();

        if (this.config().isSpawnOnJoin()) {
            p.teleport(loc.add(new Location(p.getWorld(), 0.5, 0, 0.5)));
        }
    }

    @Override
    public InputStream getResourceAsStream(String resource) {
        return this.getResource(resource);
    }

    private ConfigProvider provider;

    @Override
    public ConfigProvider getConfigProvider() {
        if (provider == null) {
            provider = new BukkitConfigProvider(this, null);
        }
        return provider;
    }

    @Override
    public void reload() {
        this.chat().log(JSONTextBuilder.text("Reloading configs...").setColor(JSONColor.GREEN));
        this.config.loadConfig();
    }

    private Map<String, ArcalCommand> commandMap = new HashMap<>();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Set<String> keySet = this.commandMap.keySet();
        for (String name : keySet) {
            if (name.equals(label.toLowerCase())) {
                ArcalCommand cmd = commandMap.get(name);
                cmd.execute(sender, args);
                break;
            }
        }
        return true;
    }

    public <T extends ArcalCommand> T registerCommand(Plugin plugin, T command) {
        if (!this.commandMap.values().contains(command)) {
            String name = command.getName().toLowerCase();
            Bukkit.getPluginCommand(name).setExecutor(this);
            this.commandMap.put(name, command);
            return command;
        } else {
            throw new IllegalArgumentException("Already registered. command=" + command.toString());
        }
    }

    @Override
    public ConfigProvider createConfigProviderByLoadingFile(File file) throws IOException {
        ConfigProvider provider = this.getConfigProvider();
        provider.load(file);
        return provider;
    }

    @Override
    public int getOnlinePlayerCount() {
        return Bukkit.getOnlinePlayers().size();
    }

    @Override
    public int getMaxPlayers() {
        return Bukkit.getMaxPlayers();
    }
}
