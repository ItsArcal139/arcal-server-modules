package cf.arcal.plugins.api.bukkit.util;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.*;
import cf.arcal.plugins.api.util.*;

public class BukkitJSONTextBuilder {
    public static BaseComponent[] build(JSONComponent text) {
        BaseComponent comp = null;
        if (text instanceof JSONTextComponent) {
            comp = buildText((JSONTextComponent) text);
        } else if (text instanceof JSONTranslatableComponent) {
            comp = buildTranslatable((JSONTranslatableComponent) text);
        } else {
            comp = buildText((JSONTextComponent) new JSONTextComponent("<invalid>").setColor(JSONColor.RED).setExtras(text.getExtras()));
        }

        comp.setItalic(text.isItalic());
        comp.setBold(text.isBold());
        comp.setObfuscated(text.isObfuscated());
        comp.setInsertion(text.getInsertion());
        comp.setUnderlined(text.isUnderlined());
        comp.setStrikethrough(text.isStrikethrough());

        if (text.getColor() != null) {
            comp.setColor(ChatColor.valueOf(text.getColor().name()));
        }

        if (text.getClickEvent() != null) {
            JSONComponent.ClickEvent event = text.getClickEvent();
            ClickEvent ev = new ClickEvent(ClickEvent.Action.valueOf(event.getAction().name()), event.getValue());
            comp.setClickEvent(ev);
        }

        if (text.getHoverEvent() != null) {
            JSONComponent.HoverEvent event = text.getHoverEvent();
            HoverEvent ev = new HoverEvent(HoverEvent.Action.valueOf(event.getAction().name()), build(event.getValue()));
            comp.setHoverEvent(ev);
        }

        for (JSONComponent child : text.getExtras()) {
            comp.addExtra(build(child)[0]);
        }

        BaseComponent[] result = new BaseComponent[1];
        result[0] = comp;
        return result;
    }

    private static TextComponent buildText(JSONTextComponent text) {
        TextComponent comp = new TextComponent();
        comp.setText(text.getText());
        return comp;
    }

    private static SelectorComponent buildSelector(JSONSelectorComponent text) {
        return new SelectorComponent(text.getSelector());
    }

    private static KeybindComponent buildKeybind(JSONKeybindComponent text) {
        return new KeybindComponent(text.getKeybind());
    }

    private static TranslatableComponent buildTranslatable(JSONTranslatableComponent text) {
        TranslatableComponent comp = new TranslatableComponent(text.getTranslate());
        for (JSONComponent item : text.getWiths()) {
            comp.addWith(build(item)[0]);
        }
        return comp;
    }

    private static ScoreComponent buildScore(JSONScoreComponent text) {
        JSONScoreComponent.Score score = text.getScore();
        return new ScoreComponent(score.getName(), score.getObjective());
    }
}
