package cf.arcal.plugins.api.bukkit.config;

import cf.arcal.plugins.api.*;
import cf.arcal.plugins.api.bukkit.*;
import cf.arcal.plugins.api.config.*;

import java.io.*;
import java.util.*;

import cf.arcal.plugins.api.util.JSONColor;
import cf.arcal.plugins.api.util.JSONTextBuilder;
import org.bukkit.configuration.*;

/**
 * @author Arcal
 */
public class ArcalAPIBukkitConfig extends BaseAPIConfig<Configuration> {
    private static ArcalAPIBukkitConfig instance = null;

    public static ArcalAPIBukkitConfig getInstance() {
        if (instance == null) {
            instance = new ArcalAPIBukkitConfig();
        }
        return instance;
    }

    public static void resetInstance() {
        instance = null;
    }

    @Override
    public ArcalAPIPlatform getAPI() {
        return ArcalAPIBukkit.getInstance();
    }

    private File configFile;
    private boolean cancelItemMerging = false;
    private boolean spawnOnJoin = false;

    private ArcalAPIBukkitConfig() {
        this.loadConfig();
    }

    @Override
    public final boolean loadConfig() {
        ArcalAPIBukkit api = ArcalAPIBukkit.getInstance();
        boolean prevResult = super.loadConfig();

        if (prevResult && this.config != null) {
            this.cancelItemMerging = config.getBoolean("cancel-item-merging", false);
            this.spawnOnJoin = config.getBoolean("spawn-on-join", false);
        } else {
            return false;
        }

        api.chat().log(JSONTextBuilder.text("Configuration loaded!").setColor(JSONColor.GREEN));
        return true;
    }

    public boolean doesCancelItemMerging() {
        return this.cancelItemMerging;
    }

    public boolean isSpawnOnJoin() {
        return this.spawnOnJoin;
    }

    private List<String> getStringList(String key) {
        List<String> result = this.config.getStringList(key);
        if (result == null || result.isEmpty()) {
            result = new ArrayList<>();
            result.add(this.config.getString(key));
        }
        return result;
    }

    @Override
    public void saveConfig() {
        // ;
    }
}
