package cf.arcal.plugins.api.bukkit.config;

import cf.arcal.plugins.api.config.*;
import cf.arcal.plugins.api.bukkit.*;

import java.io.*;

import org.bukkit.configuration.*;
import org.bukkit.configuration.file.*;

/**
 * @author Arcal
 */
public class BukkitConfigProvider extends ConfigProvider<Configuration> {
    public BukkitConfigProvider(ArcalAPIBukkit api, Configuration config) {
        super(api, config);
    }

    @Override
    public Configuration load(File file) {
        return YamlConfiguration.loadConfiguration(file);
    }

    @Override
    public String getString(String name) {
        return this.getConfig().getString(name);
    }

    @Override
    public void setString(String name, String value) {
        this.getConfig().set(name, value);
    }

}
