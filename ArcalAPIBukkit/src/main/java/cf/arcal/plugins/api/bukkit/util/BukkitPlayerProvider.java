package cf.arcal.plugins.api.bukkit.util;

import cf.arcal.plugins.api.util.*;
import org.bukkit.entity.*;

public class BukkitPlayerProvider extends PlayerProvider<Player> {
    private Player player;

    public BukkitPlayerProvider(Player p) {
        this.player = p;
    }

    @Override
    public Player getPlayer() {
        return this.getBukkitPlayer();
    }

    @Override
    public void sendMessage(String msg) {
        this.player.sendMessage(msg);
    }

    public Player getBukkitPlayer() {
        return this.player;
    }
}
