package cf.arcal.plugins.api.bukkit.util;

import java.util.*;

import org.bukkit.*;
import org.bukkit.entity.*;

public abstract class BannerServiceBase {
    public abstract void setLinesForPlayer(Player p, List<String> lines);

    public static boolean isTitleManagerAvailable() {
        return Bukkit.getPluginManager().getPlugin("TitleManager") != null;
    }
}
