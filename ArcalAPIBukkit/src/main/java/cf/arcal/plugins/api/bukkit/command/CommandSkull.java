package cf.arcal.plugins.api.bukkit.command;

import cf.arcal.plugins.api.bukkit.ArcalAPIBukkit;
import cf.arcal.plugins.api.util.JSONColor;
import cf.arcal.plugins.api.util.JSONTextBuilder;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.*;
import org.bukkit.command.*;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.UUID;

public class CommandSkull extends ArcalCommand {
    private ArcalAPIBukkit api = ArcalAPIBukkit.getInstance();

    private static CommandSkull instance = null;

    public static CommandSkull getInstance() {
        if (instance == null) {
            instance = new CommandSkull();
        }
        return instance;
    }

    private CommandSkull() {
        super("skull", "arcalapi.cmduse.skull", "head");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            this.showHelp(sender, args);
            return;
        }

        String subcmd = args[0];
        switch (subcmd) {
            case "help":
                this.showHelp(sender, args);
                break;
            case "me":
                if (sender instanceof Player) {
                    this.giveSkull(sender, new String[]{
                        ((Player) sender).getUniqueId().toString()
                    });
                } else {
                    api.chat().sendToSender(sender, JSONTextBuilder.text("You are not a player!").setColor(JSONColor.RED));
                    this.showHelp(sender, args);
                }
                break;
            default:
                this.giveSkull(sender, args);
                break;
        }
    }

    @Override
    public boolean execute(CommandSender cs, String string, String[] strings) {
        this.execute(cs, strings);
        return true;
    }

    private void giveSkull(CommandSender sender, String[] args) {
        if (!(sender instanceof Player)) {
            api.chat().sendToSender(sender, JSONTextBuilder.text("Only players can execute this command.").setColor(JSONColor.RED));
            return;
        }
        Player p = (Player) sender;
        String uuid = args[0];

        try {
            ItemStack skull = null;
            if (Bukkit.getVersion().contains("v1_13_")) {
                skull = new ItemStack(Material.getMaterial("PLAYER_HEAD"));
            } else {
                skull = new ItemStack(Material.SKULL_ITEM);
                skull.setDurability((short) 3);
            }

            SkullMeta meta = (SkullMeta) skull.getItemMeta();
            UUID uuidObj = UUID.fromString(uuid);

            OfflinePlayer of = Bukkit.getOfflinePlayer(uuidObj);
            meta.setOwningPlayer(of);
            skull.setItemMeta(meta);

            p.getInventory().addItem(skull);
            api.chat().log(
                JSONTextBuilder.translatable("Gave %s a skull of %s.")
                    .addWith(api.getPlayerComponent(p).setColor(JSONColor.GOLD))
                    .addWith(JSONTextBuilder.text(of.getName()).setColor(JSONColor.GOLD))
                    .addExtra(
                        JSONTextBuilder.translatable(" (UUID => %s)")
                            .addWith(JSONTextBuilder.text(uuidObj.toString()))
                            .setColor(JSONColor.GRAY)
                    )
            );

            api.chat().sendToPlayer(p,
                JSONTextBuilder.text("The skull is now yours! ")
                    .setColor(JSONColor.GREEN)
                    .addExtra(
                        JSONTextBuilder.text("Enjoy!")
                            .setColor(JSONColor.WHITE)
                    )
            );
        } catch (IllegalArgumentException ex) {
            if (ex.getMessage().contains("Invalid UUID string: ")) {
                api.chat().sendToPlayer(p, JSONTextBuilder.text("The given string is not a valid UUID.").setColor(JSONColor.RED));
                api.chat().sendToPlayer(p, JSONTextBuilder.text("Check the value you typed, or if you are getting your own").setColor(JSONColor.WHITE));
                api.chat().sendToPlayer(p,
                    JSONTextBuilder.translatable("skull, typing %s is recommended.")
                        .addWith(JSONTextBuilder.text("/skull me").setColor(JSONColor.GOLD))
                        .setColor(JSONColor.WHITE)
                );
            }
        } catch (Exception ex) {
            api.chat().sendToPlayer(p, JSONTextBuilder.text("I met trouble getting the skull to you.").setColor(JSONColor.RED));
            api.chat().sendToPlayer(p, JSONTextBuilder.text("That means, some unexpected error occurred.").setColor(JSONColor.RED));
            if (p.isOp()) {
                api.chat().sendToPlayer(p, JSONTextBuilder.text("Use the /give command instead, ...or take the look of the log?").setColor(JSONColor.RED));
            }
            ex.printStackTrace();
        }
    }

    private void showHelp(CommandSender sender, String[] args) {
        ArcalAPIBukkit api = ArcalAPIBukkit.getInstance();
        api.chat().sendToSender(sender, JSONTextBuilder.text("Usage:"));

        String format = "&6  %s &f- %s";
        if (sender.hasPermission("arcalapi.cmdsee.skull"))
            api.chat().sendToSender(sender,
                JSONTextBuilder.translatable(format)
                    .addWith(JSONTextBuilder.text("/" + this.getName() + " <uuid>").setColor(JSONColor.GOLD))
                    .addWith(JSONTextBuilder.text("Get a player head by the given UUID."))
                    .alternativeColorCode('&').setColor(JSONColor.WHITE)
            );
    }
}
