package cf.arcal.plugins.bukkit.eventlogger;

import cf.arcal.plugins.api.bukkit.ArcalAPIBukkit;
import cf.arcal.plugins.api.bukkit.command.ArcalCommand;
import cf.arcal.plugins.api.bukkit.util.BukkitJSONTextBuilder;
import cf.arcal.plugins.api.bukkit.util.NamespacedString;
import cf.arcal.plugins.api.bukkit.util.StringUtility;
import cf.arcal.plugins.api.util.*;
import cf.arcal.plugins.bukkit.eventlogger.command.CommandLogger;
import cf.arcal.plugins.bukkit.eventlogger.config.AdvancementMappingConfig;
import cf.arcal.plugins.bukkit.eventlogger.config.DeathCauseMappingConfig;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameRule;
import org.bukkit.World;
import org.bukkit.advancement.Advancement;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerAdvancementDoneEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.projectiles.BlockProjectileSource;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.scheduler.BukkitTask;

import java.net.URL;
import java.util.*;

public class ArcalEventLogger extends JavaPlugin implements Listener {

    public static class KillStreakMap extends HashMap<Player, Integer> {
        public int put(Player killer, int amount) {
            Integer result = super.put(killer, amount);
            if (result != null) return result;
            return -1;
        }

        public int add(Player killer) {
            return this.add(killer, 1);
        }

        public int get(Player killer) {
            Integer result = super.get(killer);
            if(result == null) return 0;
            return result;
        }

        public int add(Player killer, int amount) {
            int _amount = this.get(killer);
            return this.put(killer, amount + _amount);
        }
    }

    private ArcalAPIBukkit api;
    private static ArcalEventLogger instance;
    private AdvancementMappingConfig mappingConfig = null;
    private DeathCauseMappingConfig deathConfig = null;

    private Map<Player, Integer> playerTimeoutMap = new HashMap<>();
    private KillStreakMap killStreakMap = new KillStreakMap();

    public static ArcalEventLogger getInstance() {
        return instance;
    }

    @Override
    public void onDisable() {
        instance = null;
    }

    @Override
    public void onEnable() {
        instance = this;
        Plugin arcalApi = Bukkit.getPluginManager().getPlugin("ArcalAPI");
        if (arcalApi == null) {
            Bukkit.getConsoleSender().sendMessage(
                ChatColor.translateAlternateColorCodes('&', "&cArcalAPIBukkit not found! Disabling the plugin...")
            );
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }
        this.api = (ArcalAPIBukkit) arcalApi;

        Bukkit.getPluginManager().registerEvents(this, this);
        api.registerCommand(this, CommandLogger.getInstance());

        this.mappingConfig = new AdvancementMappingConfig(this);
        mappingConfig.loadConfig();

        this.deathConfig = new DeathCauseMappingConfig(this);
        deathConfig.loadConfig();

        BukkitTask timerTask = Bukkit.getScheduler().runTaskTimer(this, () -> {
            for (Player p : playerTimeoutMap.keySet()) {
                playerTimeoutMap.put(p, playerTimeoutMap.get(p) - 1);

                if (playerTimeoutMap.get(p) == 0) {
                    Bukkit.getScheduler().runTask(this, () -> playerTimeoutMap.remove(p));
                    if (p.isOnline()) {
                        api.chat().log(
                            JSONTextBuilder.translatable("Well, I might have saved %s from being kicked due to Internet issue!")
                                .addWith(JSONTextBuilder.text(p.getName()).setColor(JSONColor.GOLD))
                                .setColor(JSONColor.WHITE)
                        );
                    } else {
                        api.chat().log(
                            JSONTextBuilder.translatable("Oops. %s failed to survive form the ping war.")
                                .addWith(JSONTextBuilder.text(p.getName()).setColor(JSONColor.GOLD))
                                .setColor(JSONColor.WHITE)
                        );
                    }
                }
            }
        }, 1, 0);
    }

    public ArcalAPIBukkit getAPI() {
        return this.api;
    }

    public void reload() {
        this.mappingConfig.loadConfig();
        this.deathConfig.loadConfig();
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        Player p = e.getEntity();
        api.chat().log(
            new JSONTranslatableComponent("%s recently died at %s")
                .addWith(api.getPlayerComponent(p))
                .addWith(
                    new JSONTextComponent(StringUtility.locToString(p.getLocation())).setColor(JSONColor.GOLD)
                )
        );

        Player killer = p.getKiller();


        if (this.deathConfig().doesUseCustomDeathCause()) {
            String deathMsg = e.getDeathMessage();
            List<JSONComponent> withs = new ArrayList<>();

            String deathCauseFormat = deathMsg;
            deathCauseFormat = deathCauseFormat.replace(p.getName(), "%s");
            withs.add(api.getPlayerComponent(p));

            EntityDamageEvent de = p.getLastDamageCause();
            if (de instanceof EntityDamageByEntityEvent) {
                EntityDamageByEntityEvent dbeEvent = (EntityDamageByEntityEvent) de;
                Entity damager = dbeEvent.getDamager();
                if (damager instanceof Arrow) {
                    // Damager is arrow. Check the shooter.
                    if (((Arrow) damager).getShooter() instanceof LivingEntity) {
                        // Shoot by an entity.
                        LivingEntity le = (LivingEntity) ((Arrow) damager).getShooter();
                        deathCauseFormat = deathCauseFormat.replace(le.getName(), "%s");
                        if (le instanceof Player) {
                            withs.add(api.getPlayerComponent((Player) le));
                        } else {
                            withs.add(JSONTextBuilder.text(le.getName()).setColor(JSONColor.GRAY));
                        }

                        // Try to replace the item name to "%s".
                        ItemStack item = le.getEquipment().getItemInMainHand();
                        if (item != null && item.getItemMeta() != null) {
                            api.chat().log(
                                JSONTextBuilder.text("Item holding in main hand -> ")
                                    .addExtra(JSONTextBuilder.text(item.toString()).setColor(JSONColor.GOLD))
                            );
                            String displayName = item.getItemMeta().getDisplayName();
                            if (displayName != null) {
                                deathCauseFormat = deathCauseFormat.replace("[" + displayName + "]", "%s");
                                withs.add(
                                    api.getItemComponent(item, displayName, "[%s]")
                                        .setColor(item.getItemMeta().hasEnchants() ? JSONColor.AQUA : JSONColor.GOLD)
                                );
                            }
                        }

                    } else {
                        // Block shooter. Returns the arrow name.
                        deathCauseFormat = deathCauseFormat.replace(damager.getName(), "%s");
                        withs.add(JSONTextBuilder.text(damager.getName()).setColor(JSONColor.GRAY));
                    }
                } else {
                    // Returns the damager name, even if it is a projectile.
                    deathCauseFormat = deathCauseFormat.replace(damager.getName(), "%s");

                    if (damager instanceof Player) {
                        withs.add(api.getPlayerComponent((Player) damager).setColor(JSONColor.GRAY));
                    } else {
                        withs.add(JSONTextBuilder.translatable("entity.minecraft." + damager.getType().name().toLowerCase()).setColor(JSONColor.GRAY));
                    }

                    if (damager instanceof LivingEntity) {
                        // Try to replace the item name to "%s".
                        ItemStack item = ((LivingEntity) damager).getEquipment().getItemInMainHand();
                        if (item != null && item.getItemMeta() != null) {
                            api.chat().log(
                                JSONTextBuilder.text("Item holding in main hand -> ")
                                    .addExtra(JSONTextBuilder.text(item.toString()).setColor(JSONColor.GOLD))
                            );
                            String displayName = item.getItemMeta().getDisplayName();
                            if (displayName != null) {
                                deathCauseFormat = deathCauseFormat.replace("[" + displayName + "]", "%s");
                                withs.add(
                                    api.getItemComponent(item, displayName, "[%s]")
                                        .setColor(item.getItemMeta().hasEnchants() ? JSONColor.AQUA : JSONColor.GOLD)
                                );
                            }
                        }
                    }
                }
            } else {
                String igdConst = "[Intentional Game Design]";

                if (deathCauseFormat.contains(igdConst)) {
                    // Intentional Game Design
                    deathCauseFormat = deathCauseFormat.replace(igdConst, "%s");

                    String translated = this.deathConfig.getMapping().get(igdConst);
                    if (translated == null) {
                        translated = igdConst;
                    }
                    withs.add(
                        JSONTextBuilder.translatable(translated)
                            .alternativeColorCode('&')
                            .setHoverEvent(JSONComponent.HoverEvent.showText(JSONTextBuilder.text("MCPE-28723")))
                            .setClickEvent(JSONComponent.ClickEvent.openUrl("https://bugs.mojang.com/browse/MCPE-28723"))
                    );
                } else if (p.getKiller() != null) {
                    // Fight death. Get the killer.
                    Player k = p.getKiller();
                    deathCauseFormat = deathCauseFormat.replace(k.getName(), "%s");
                    withs.add(
                        api.getPlayerComponent(k)
                    );
                }
                api.chat().log(JSONTextBuilder.text(de.toString()));
            }

            api.chat().log(
                JSONTextBuilder.text("Death message is using format -> ")
                    .addExtra(JSONTextBuilder.text(deathCauseFormat).setColor(JSONColor.GOLD))
            );

            String translated = this.deathConfig.getMapping().get(deathCauseFormat);
            if (translated == null) {
                translated = deathCauseFormat;
            }

            JSONComponent comp = JSONTextBuilder.translatable(translated)
                .setWiths(withs)
                .alternativeColorCode('&')
                .setColor(JSONColor.WHITE);

            if (killer != null) {
                this.killStreakMap.add(killer);
            }
            int killedStreak = this.killStreakMap.get(p);
            this.killStreakMap.put(p, 0);

            if (this.deathConfig.isKillStreakEnabled() && killer != null) {
                int ks = this.killStreakMap.get(killer);
                if (ks > 1) {
                    comp.addExtra(
                        JSONTextBuilder.text(String.format(this.deathConfig.getKillStreakFormat(), ks))
                            .alternativeColorCode('&')
                    );
                }
            }

            if (this.deathConfig.isShutdownEnabled() && killedStreak >= this.deathConfig.getShutdownThreshold()) {
                comp.addExtra(
                    JSONTextBuilder.text(this.deathConfig.getShutdownFormat())
                        .alternativeColorCode('&')
                );
            }

            // A better code than Bukkit.getPlayer() -> foreach.
            for (World w : Bukkit.getWorlds()) {
                boolean orig = w.getGameRuleValue(GameRule.SHOW_DEATH_MESSAGES);
                w.setGameRule(GameRule.SHOW_DEATH_MESSAGES, false);
                for (Player pp : w.getPlayers()) {
                    api.chat().sendToPlayer(pp, comp);
                }
                Bukkit.getScheduler().runTaskLater(this, () -> {
                    w.setGameRule(GameRule.SHOW_DEATH_MESSAGES, orig);
                }, 1);
            }

            if (this.deathConfig.doesReplaceVanillaMsg()) {
                e.setDeathMessage(BukkitJSONTextBuilder.build(comp)[0].toLegacyText());
            }
        }
    }

    @EventHandler
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        if (e.getEntity().getType() == EntityType.PLAYER) return;

        if (e.getEntity() instanceof LivingEntity) {
            LivingEntity le = (LivingEntity) e.getEntity();

            // Lightning strike detect.
            if (e.getDamager().getType() == EntityType.LIGHTNING) {
                String killedName = le.getName();

                api.chat().log(
                    JSONTextBuilder.translatable("%s was strike by lightning at %s")
                        .addWith(
                            JSONTextBuilder.text(killedName)
                                .addExtra(JSONTextBuilder.translatable("").addWith(JSONTextBuilder.text(le.getType().toString())))
                                .setColor(JSONColor.GOLD)
                        ) // The entity being strike.
                        .addWith(JSONTextBuilder.text(StringUtility.locToString(le.getLocation())).setColor(JSONColor.GOLD)) // Location
                        .addExtra(
                            JSONTextBuilder.translatable(" (Last damage: %s)")
                                .addWith(JSONTextBuilder.text(String.format("%.2f", e.getFinalDamage())))
                                .setColor(JSONColor.GRAY)
                        )
                );
            }

            // Death detect.
            if (e.getFinalDamage() > le.getHealth() && !e.isCancelled()) {
                if (e.getDamager() instanceof Projectile) {
                    Projectile pr = (Projectile) e.getDamager();
                    ProjectileSource from = pr.getShooter();

                    String sourceName = "ProjectileSource";
                    String sourceType = "ProjectileSource";
                    if (from instanceof LivingEntity) {
                        // Safe?
                        sourceName = ((LivingEntity) from).getName();
                        sourceType = ((LivingEntity) from).getType().toString();
                    } else if (from instanceof BlockProjectileSource) {
                        sourceName = ((BlockProjectileSource) from).getBlock().toString();
                        sourceType = ((BlockProjectileSource) from).getBlock().getType().toString();
                    }

                    String damagerName = e.getDamager().getName();
                    String killedName = le.getName();

                    api.chat().log(
                        JSONTextBuilder.translatable("%s was killed by %s at %s")
                            .addWith(
                                JSONTextBuilder.text(killedName)
                                    .setColor(JSONColor.GOLD)
                                    .addExtra(
                                        JSONTextBuilder.translatable(" (%s)")
                                            .addWith(JSONTextBuilder.text(le.getType().toString()))
                                    )
                            )
                            .addWith(
                                JSONTextBuilder.text(damagerName)
                                    .setColor(JSONColor.GOLD)
                                    .addExtra(
                                        JSONTextBuilder.translatable(" (shoot by %s, %s)")
                                            .addWith(JSONTextBuilder.text(sourceName))
                                            .addWith(JSONTextBuilder.text(sourceType))
                                    )
                            )
                            .addWith(JSONTextBuilder.text(StringUtility.locToString(le.getLocation())).setColor(JSONColor.GOLD))
                            .addExtra(
                                JSONTextBuilder.translatable(" (Last damage: %s)")
                                    .addWith(JSONTextBuilder.text(String.format("%.2f", e.getFinalDamage())))
                                    .setColor(JSONColor.GRAY)
                            )
                    );
                } else {
                    String damagerName = e.getDamager().getName();
                    String killedName = le.getName();

                    api.chat().log(
                        JSONTextBuilder.translatable("%s was killed by %s at %s")
                            .addWith(
                                JSONTextBuilder.text(killedName)
                                    .setColor(JSONColor.GOLD)
                                    .addExtra(
                                        JSONTextBuilder.translatable(" (%s)")
                                            .addWith(JSONTextBuilder.text(le.getType().toString()))
                                    )
                            )
                            .addWith(
                                JSONTextBuilder.text(damagerName)
                                    .setColor(JSONColor.GOLD)
                                    .addExtra(
                                        JSONTextBuilder.translatable(" (%s)")
                                            .addWith(JSONTextBuilder.text(e.getDamager().getType().toString()))
                                    )
                            )
                            .addWith(JSONTextBuilder.text(StringUtility.locToString(le.getLocation())).setColor(JSONColor.GOLD))
                            .addExtra(
                                JSONTextBuilder.translatable(" (Last damage: %s)")
                                    .addWith(JSONTextBuilder.text(String.format("%.2f", e.getFinalDamage())))
                                    .setColor(JSONColor.GRAY)
                            )
                    );
                }
            }
        }
    }

    public AdvancementMappingConfig mappingConfig() {
        return this.mappingConfig;
    }

    public DeathCauseMappingConfig deathConfig() {
        return this.deathConfig;
    }

    @EventHandler
    @SuppressWarnings("unused")
    public void onAdvancement(PlayerAdvancementDoneEvent event) {
        Advancement advancement = event.getAdvancement();
        Player p = event.getPlayer();

        String key = advancement.getKey().getKey();
        String name = key;

        if (name.startsWith("recipes")) {
            // Ignore recipe unlocks.
            return;
        }

        // How do I convert the name?
        ChatColor color = ChatColor.GREEN;

        String prettified;
        HashMap<String, AdvancementMappingConfig.MapOption> map = mappingConfig.getMapping();
        Set<String> keys = map.keySet();
        boolean hidden = true;

        if (keys.contains(name)) {
            prettified = NamespacedString.prettify(name);

            AdvancementMappingConfig.MapOption option = map.get(name);
            name = option.getReplaceTo();
            color = option.getColor();
            hidden = option.isHidden();
        } else {
            name = NamespacedString.prettify(name);
            if (!this.mappingConfig().doesUseCustomAdvancementName()) {
                String[] path = name.split(" -> ");
                name = path[path.length - 1];
            }
            prettified = name;
        }

        JSONTranslatableComponent comp = new JSONTranslatableComponent(this.mappingConfig.getAdvancementDoneMsg());

        String nameKey = "advancements." + StringUtility.joinArray(key.split("/"), ".") + ".title";
        String descKey = "advancements." + StringUtility.joinArray(key.split("/"), ".") + ".description";

        String format = this.mappingConfig.getAdvancementNameFormat();

        JSONComponent titleComponent;
        if (this.mappingConfig().doesUseCustomAdvancementName()) {
            titleComponent = JSONTextBuilder.text(name);
        } else {
            titleComponent = JSONTextBuilder.translatable(nameKey);
        }

        comp.addWith(api.getPlayerComponent(p))
            .addWith(
                new JSONTranslatableComponent(format)                                                   // Format like "[%s]"
                    .addWith(new JSONTranslatableComponent(nameKey))                                    // Fill in the name.
                    .setHoverEvent(JSONComponent.HoverEvent.showText(                                   // Show details when hover
                        titleComponent.setColor(JSONColor.valueOf(color.name()))                        // Title with color
                            .addExtra(new JSONTextComponent("\n"))                                      // Next line
                            .addExtra(new JSONTranslatableComponent(descKey).setColor(JSONColor.WHITE)) // Description (white)
                    ))
                    .setColor(JSONColor.valueOf(color.name()))
            ).alternativeColorCode('&');

        if (!hidden) {
            // A better code than Bukkit.getPlayer() -> foreach.
            for (World w : Bukkit.getWorlds()) {
                boolean orig = w.getGameRuleValue(GameRule.ANNOUNCE_ADVANCEMENTS);
                w.setGameRule(GameRule.ANNOUNCE_ADVANCEMENTS, false);
                for (Player e : w.getPlayers()) {
                    api.chat().sendToPlayer(e, comp);
                }
                Bukkit.getScheduler().runTaskLater(this, () -> {
                    w.setGameRule(GameRule.ANNOUNCE_ADVANCEMENTS, orig);
                }, 1);
            }
        }

        api.chat().log(
            JSONTextBuilder.translatable("%s has made the achievement %s.")
                .addWith(api.getPlayerComponent(p))
                .addWith(
                    JSONTextBuilder.text(String.format("[%s]", prettified))
                        .setColor(JSONColor.valueOf(color.name()))
                )
                .addExtra(
                    JSONTextBuilder.translatable(" (Mapped name: \"%s\")")
                        .addWith(JSONTextBuilder.text(name))
                )
        );
    }


    /**
     * Called when a player get kicked.
     *
     * @param e The {@code PlayerKickEvent}.
     */
    @EventHandler
    public void onPlayerKicked(PlayerKickEvent e) {
        if (e.getReason().equals("Timed out")) {
            Player p = e.getPlayer();
            if (!playerTimeoutMap.containsKey(p)) {
                api.chat().log(
                    new JSONTranslatableComponent("Stopping %s from being kicked due to connection timed out!")
                        .addWith(api.getPlayerComponent(p))
                        .setColor(JSONColor.WHITE)
                );
            }
            playerTimeoutMap.put(p, 20);
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerQuitted(PlayerQuitEvent e) {
        this.killStreakMap.remove(e.getPlayer());
    }
}
