package cf.arcal.plugins.bukkit.eventlogger.config;

import cf.arcal.plugins.api.bukkit.*;
import cf.arcal.plugins.api.bukkit.util.*;
import cf.arcal.plugins.api.util.JSONColor;
import cf.arcal.plugins.api.util.JSONTextBuilder;
import cf.arcal.plugins.api.util.JSONTextComponent;
import cf.arcal.plugins.bukkit.eventlogger.ArcalEventLogger;
import org.bukkit.ChatColor;
import org.bukkit.configuration.*;
import org.bukkit.configuration.file.*;

import java.io.*;
import java.nio.file.*;
import java.util.*;

public class AdvancementMappingConfig {
    private ArcalEventLogger plugin;
    private ArcalAPIBukkit api;
    private File configFile;

    private static final String FILE_NAME = "advancement-mapping.yml";

    private HashMap<String, MapOption> dictionary = new HashMap<>();

    private boolean useCustomAdvancementName = false;
    private String advancementDoneMsg = null;
    private String advancementNameFormat = null;

    public AdvancementMappingConfig(ArcalEventLogger plugin) {
        this.api = plugin.getAPI();
        this.plugin = plugin;
        this.configFile = new File(plugin.getDataFolder(), FILE_NAME);
    }

    public void loadConfig() {
        if (!plugin.getDataFolder().exists()) {
            api.chat().log(new JSONTextComponent("Data folder doesn't exist. Creating one..."));
            plugin.getDataFolder().mkdir();
        }

        if (!this.configFile.exists()) {
            try (InputStream in = plugin.getResource(FILE_NAME)) {
                api.chat().log(JSONTextBuilder.text("Copying the default advancement mapping to the disk...").setColor(JSONColor.GOLD));
                Files.copy(in, this.configFile.toPath());
            } catch (IOException ex) {
                api.chat().log(JSONTextBuilder.text("Failed to copy default advancement mapping onto the disk!").setColor(JSONColor.RED));
                ex.printStackTrace();
            }
        }

        YamlConfiguration config = YamlConfiguration.loadConfiguration(this.configFile);
        this.useCustomAdvancementName = config.getBoolean("use-custom-advancement-name", false);
        this.advancementDoneMsg = config.getString("advancement-done-message", "%1$s&r has made the achievement %2$s");
        this.advancementNameFormat = config.getString("advancement-name-format", "[%s]");

        this.dictionary = new HashMap<>();
        ConfigurationSection section = config.getConfigurationSection("mapping");

        Set<String> entryKeys = section.getKeys(false);
        for (String key : entryKeys) {
            String name = section.getString(key + ".name");
            if (name != null) {
                String replace = section.getString(key + ".to", NamespacedString.prettify(name));
                ChatColor color = ChatColor.getByChar(section.getString(key + ".color", "a").substring(0, 1));
                boolean hidden = section.getBoolean(key + ".hidden", false);
                this.dictionary.put(name, new MapOption(replace, color, hidden));
            }
        }
        api.chat().log(JSONTextBuilder.text("Advancement mapping loaded!").setColor(JSONColor.GREEN));
    }

    public class MapOption {
        private String replaceTo;
        private ChatColor color;
        private boolean hidden;

        public MapOption(String replaceTo, ChatColor color, boolean isHidden) {
            this.color = color;
            this.replaceTo = replaceTo;
            this.hidden = isHidden;
        }

        public String getReplaceTo() {
            return this.replaceTo;
        }

        public ChatColor getColor() {
            return this.color;
        }

        public boolean isHidden() {
            return this.hidden;
        }
    }

    public HashMap<String, MapOption> getMapping() {
        return this.dictionary;
    }

    public boolean doesUseCustomAdvancementName() {
        return this.useCustomAdvancementName;
    }

    public String getAdvancementDoneMsg() {
        return this.advancementDoneMsg;
    }

    public String getAdvancementNameFormat() {
        return this.advancementNameFormat;
    }
}
