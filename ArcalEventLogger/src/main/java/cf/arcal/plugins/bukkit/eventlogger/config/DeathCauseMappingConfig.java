package cf.arcal.plugins.bukkit.eventlogger.config;

import cf.arcal.plugins.api.bukkit.ArcalAPIBukkit;
import cf.arcal.plugins.api.bukkit.util.NamespacedString;
import cf.arcal.plugins.api.util.JSONColor;
import cf.arcal.plugins.api.util.JSONTextBuilder;
import cf.arcal.plugins.api.util.JSONTextComponent;
import cf.arcal.plugins.bukkit.eventlogger.ArcalEventLogger;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Set;

public class DeathCauseMappingConfig {
    private ArcalEventLogger plugin;
    private ArcalAPIBukkit api;
    private File configFile;

    private static final String FILE_NAME = "death-cause.yml";

    private HashMap<String, String> dictionary = new HashMap<>();

    private boolean useCustomDeathCause = false;
    private boolean replaceVanillaMsg = false;
    private boolean killStreakEnabled = false;
    private String killStreakFormat = "§b§l - §b§l%s§b§l KILL STREAK!!";

    private boolean shutdownEnabled = false;
    private int shutdownThreshold = 5;
    private String shutdownFormat = "§6§l - §6§lSHUTDOWN!!";

    public DeathCauseMappingConfig(ArcalEventLogger plugin) {
        this.api = plugin.getAPI();
        this.plugin = plugin;
        this.configFile = new File(plugin.getDataFolder(), FILE_NAME);
    }

    public void loadConfig() {
        if (!plugin.getDataFolder().exists()) {
            api.chat().log(new JSONTextComponent("Data folder doesn't exist. Creating one..."));
            plugin.getDataFolder().mkdir();
        }

        if (!this.configFile.exists()) {
            try (InputStream in = plugin.getResource(FILE_NAME)) {
                api.chat().log(JSONTextBuilder.text("Copying the default death cause mapping to the disk...").setColor(JSONColor.GOLD));
                Files.copy(in, this.configFile.toPath());
            } catch (IOException ex) {
                api.chat().log(JSONTextBuilder.text("Failed to copy default death cause mapping onto the disk!").setColor(JSONColor.RED));
                ex.printStackTrace();
            }
        }

        YamlConfiguration config = YamlConfiguration.loadConfiguration(this.configFile);
        this.useCustomDeathCause = config.getBoolean("use-custom-death-cause", false);
        this.killStreakEnabled = config.getBoolean("enable-kill-streak", false);
        this.killStreakFormat = config.getString("kill-streak-format", killStreakFormat);
        this.replaceVanillaMsg = config.getBoolean("replace-vanilla-msg", false);

        this.shutdownEnabled = config.getBoolean("enable-shutdown", false);
        this.shutdownThreshold = config.getInt("shutdown-threshold", 5);
        this.shutdownFormat = config.getString("shutdown-format", shutdownFormat);

        this.dictionary = new HashMap<>();
        ConfigurationSection section = config.getConfigurationSection("mapping");

        Set<String> entryKeys = section.getKeys(false);
        for (String key : entryKeys) {
            String format = section.getString(key);
            if (format != null) {
                api.chat().log(JSONTextBuilder.text("Registered format by key => ").addExtra(JSONTextBuilder.text(key).setColor(JSONColor.GOLD)));
                this.dictionary.put(key, format);
            }
        }
        api.chat().log(JSONTextBuilder.text("Death cause mapping loaded!").setColor(JSONColor.GREEN));
    }

    public HashMap<String, String> getMapping() {
        return this.dictionary;
    }

    public boolean isKillStreakEnabled() {
        return this.killStreakEnabled;
    }

    public boolean doesUseCustomDeathCause() {
        return this.useCustomDeathCause;
    }

    public boolean doesReplaceVanillaMsg() {
        return this.replaceVanillaMsg;
    }

    public String getKillStreakFormat() {
        return this.killStreakFormat;
    }

    public boolean isShutdownEnabled() {
        return this.shutdownEnabled;
    }

    public int getShutdownThreshold() {
        return this.shutdownThreshold;
    }

    public String getShutdownFormat() {
        return this.shutdownFormat;
    }
}
