package cf.arcal.plugins.api.game;

import cf.arcal.plugins.api.bukkit.ArcalAPIBukkit;
import cf.arcal.plugins.api.game.util.GameError;
import cf.arcal.plugins.api.util.JSONColor;
import cf.arcal.plugins.api.util.JSONTextBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import java.util.Collection;

public class ArcalGameAPI extends JavaPlugin implements Listener {
    public static final int TICK_TO_SEC = 20;

    private static ArcalGameAPI instance = null;
    private ArcalAPIBukkit api;
    private ArcalGame game = null;
    private BukkitTask timerTask = null;
    private int timer = -1;

    public ArcalAPIBukkit getApi() {
        return this.api;
    }

    @Override
    public void onEnable() {
        instance = this;
        Plugin arcalApi = Bukkit.getPluginManager().getPlugin("ArcalAPI");
        if (arcalApi == null) {
            Bukkit.getConsoleSender().sendMessage(
                ChatColor.translateAlternateColorCodes('&', "&cArcalAPIBukkit not found! Disabling the plugin...")
            );
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }
        this.api = (ArcalAPIBukkit) arcalApi;

        Bukkit.getPluginManager().registerEvents(this, this);

        this.timerTask = Bukkit.getScheduler().runTaskTimer(this, () -> {
            this.tick(this.timer);

            if(this.timer < 0 && this.shouldIncTick) {
                if(-this.timer % 20 == 0) {
                    int countdownSec = this.timer / -20;
                    if(this.timer == -game.getMaxCountdownTicks() || countdownSec <= 5 || (countdownSec <= 30 && countdownSec % 5 == 0)) {
                        api.chat().broadcast(
                            JSONTextBuilder.translatable("The game starts in %s seconds!")
                                .addWith(JSONTextBuilder.text(String.valueOf(countdownSec)).setColor(
                                    countdownSec <= 5 ? JSONColor.RED : countdownSec <= 10 ? JSONColor.YELLOW : JSONColor.GREEN
                                ))
                                .setColor(JSONColor.YELLOW)
                        );
                    }
                }
            }

            // Shall we play now?
            if(this.timer == 0) {
                this.startGame();
            }

            // Should the timer increase by tick now?
            if(this.shouldIncTick) {
                this.timer++;
            }
        }, 0, 1);
    }

    private boolean shouldIncTick = false;

    /**
     * Tick the game timer.
     * @param timer Timer in ticks (1/20s).
     */
    public void tick(int timer) {
        if(this.game != null) {
            game.tick(timer);
        } else {
            throw GameError.notSet();
        }
    }

    public void startGame() {
        if(this.game != null) {
            game.start();
        } else {
            throw GameError.notSet();
        }
    }

    public void stopGame() {
        if(this.game != null) {
            game.stop();
            this.shouldIncTick = false;
        } else {
            throw GameError.notSet();
        }
    }

    @Override
    public void onDisable() {
        instance = null;

        if(this.game != null) {
            game.unload();
        }
    }

    public static ArcalGameAPI getInstance() {
        return instance;
    }

    public void setGame(ArcalGame game) {
        ArcalGame oldGame = this.game;
        this.game = game;

        if(oldGame != null) {
            this.stopGame();
            oldGame.unload();
        }

        this.timer = game.getMaxCountdownTicks() * -1;
        game.load();
    }

    public ArcalGame getGame() {
        return this.game;
    }

    @EventHandler
    public void onPlayerJoined(PlayerJoinEvent event) {
        if(this.game != null) {
            this.addPlayer(event.getPlayer());
            this.updatePendingAction();
        }
    }

    @EventHandler
    public void onPlayerLeft(PlayerQuitEvent event) {
        if(this.game != null) {
            this.removePlayer(event.getPlayer());
            this.updatePendingAction();
        }
    }

    private void updatePendingAction() {
        if(this.game != null) {
            Collection<Player> players = this.game.getPlayers();
            if(players.size() >= this.game.getMinPlayerCount()) {
                this.shouldIncTick = true;
            } else {
                this.shouldIncTick = false;
                this.timer = this.game.getMaxCountdownTicks() * -1;
            }
        }
    }

    public void addPlayer(Player p) {
        if(this.game != null) {
            game.addPlayer(p);
            api.chat().broadcast(
                JSONTextBuilder.translatable("%s joined the game!")
                    .addWith(api.getPlayerComponent(p))
                    .addExtra(
                        JSONTextBuilder.translatable(" (%s/%s)")
                            .addWith(JSONTextBuilder.text(String.valueOf(api.getOnlinePlayerCount())).setColor(JSONColor.AQUA))
                            .addWith(JSONTextBuilder.text(String.valueOf(api.getMaxPlayers())).setColor(JSONColor.AQUA))
                            .setColor(JSONColor.GRAY)
                    )
                    .setColor(JSONColor.YELLOW)
            );
        } else {
            throw GameError.notSet();
        }
    }

    public void removePlayer(Player p) {
        if(this.game != null) {
            game.removePlayer(p);
            api.chat().broadcast(
                JSONTextBuilder.translatable("%s left the game!")
                    .addWith(api.getPlayerComponent(p))
                    .setColor(JSONColor.YELLOW)
            );
        } else {
            throw GameError.notSet();
        }
    }
}
