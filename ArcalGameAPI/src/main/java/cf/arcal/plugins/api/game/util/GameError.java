package cf.arcal.plugins.api.game.util;

public class GameError {
    public static IllegalStateException notSet() {
        return new IllegalStateException("Game is not set!");
    }
}
