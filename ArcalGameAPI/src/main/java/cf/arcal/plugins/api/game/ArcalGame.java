package cf.arcal.plugins.api.game;

import org.bukkit.entity.Player;

import java.util.Collection;

public interface ArcalGame {
    void start();
    void stop();
    void tick(int timer);

    void addPlayer(Player p);
    void removePlayer(Player p);
    Collection<Player> getPlayers();

    void load();
    void unload();

    int getMaxGameTime();
    int getMinPlayerCount();
    int getMaxCountdownTicks();
}
