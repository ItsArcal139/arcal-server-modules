package cf.arcal.plugins.bukkit.salary.config;

import cf.arcal.plugins.api.bukkit.ArcalAPIBukkit;
import cf.arcal.plugins.api.util.*;
import cf.arcal.plugins.bukkit.salary.ArcalSalaryPlugin;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.*;

public class SalaryMappingConfig {
    private ArcalSalaryPlugin plugin;
    private ArcalAPIBukkit api;
    private File configFile;

    private static final String FILE_NAME = "salary.yml";

    private int date = 1;
    private HashMap<UUID, Double> dictionary = new HashMap<>();
    private HashMap<String, String> messages = new HashMap<>();
    private List<UUID> noSalaryUUIDs = new ArrayList<>();
    private double globalSalary = 0;

    public SalaryMappingConfig(ArcalSalaryPlugin plugin) {
        this.api = plugin.getApi();
        this.plugin = plugin;
        this.configFile = new File(plugin.getDataFolder(), FILE_NAME);
    }

    public void loadConfig() {
        if (!plugin.getDataFolder().exists()) {
            api.chat().log(new JSONTextComponent("Data folder doesn't exist. Creating one..."));
            plugin.getDataFolder().mkdir();
        }

        if (!this.configFile.exists()) {
            try (InputStream in = plugin.getResource(FILE_NAME)) {
                api.chat().log(JSONTextBuilder.text("Copying the salary mapping to the disk...").setColor(JSONColor.GOLD));
                Files.copy(in, this.configFile.toPath());
            } catch (IOException ex) {
                api.chat().log(JSONTextBuilder.text("Failed to copy salary mapping onto the disk!").setColor(JSONColor.RED));
                ex.printStackTrace();
            }
        }

        YamlConfiguration config = YamlConfiguration.loadConfiguration(this.configFile);
        this.dictionary = new HashMap<>();
        this.date = config.getInt("salary.date", 1);
        this.globalSalary = config.getDouble("salary.global", 0);


        ConfigurationSection msgSection = config.getConfigurationSection("salary.messages");
        if (msgSection != null) {
            Set<String> entryKeys = msgSection.getKeys(false);
            for (String key : entryKeys) {
                this.messages.put(key, msgSection.getString(key, key));
            }
        }

        ConfigurationSection section = config.getConfigurationSection("salary.player-exclusives");

        api.chat().log(
            new JSONTranslatableComponent("Salary day is set to the %s day of a month.")
                .addWith(new JSONTextComponent(String.format("%d", this.date)).setColor(JSONColor.GOLD))
        );

        try {
            if (section != null) {
                Set<String> entryKeys = section.getKeys(false);
                for (String key : entryKeys) {
                    UUID uuid = UUID.fromString(key);
                    double salary = section.getDouble(key, this.globalSalary);

                    JSONComponent playerComponent = null;
                    if (Bukkit.getPlayer(uuid) != null) {
                        playerComponent = api.getPlayerComponent(Bukkit.getPlayer(uuid));
                    } else {
                        playerComponent = api.getPlayerComponent(Bukkit.getOfflinePlayer(uuid));
                    }

                    api.chat().log(
                        new JSONTranslatableComponent("%s's salary is set to %s.")
                            .addWith(playerComponent)
                            .addWith(new JSONTextComponent(String.format("$%,.2f", salary)).setColor(JSONColor.GOLD))
                    );

                    this.dictionary.put(uuid, salary);
                }
                api.chat().log(
                    new JSONTranslatableComponent("Other players' salary are set to %s.")
                        .addWith(new JSONTextComponent(String.format("$%,.2f", this.globalSalary)).setColor(JSONColor.GOLD))
                );
            } else {
                api.chat().log(
                    new JSONTranslatableComponent("All players' salary are set to %s.")
                        .addWith(new JSONTextComponent(String.format("$%,.2f", this.globalSalary)).setColor(JSONColor.GOLD))
                );
            }
        } catch (IllegalArgumentException ex) {
            // ?
        }

        for (String uuid : config.getStringList("salary.players-no-salary")) {
            try {
                UUID u = UUID.fromString(uuid);
                this.noSalaryUUIDs.add(u);
            } catch (IllegalArgumentException ex) {
                api.chat().log(new JSONTextComponent("Invalid UUID in salary.players-no-salary: " + uuid).setColor(JSONColor.RED));
            }
        }
        api.chat().log(JSONTextBuilder.text("Salary mapping loaded!").setColor(JSONColor.GREEN));
    }

    public HashMap<UUID, Double> getMapping() {
        return this.dictionary;
    }

    public double getGlobalSalary() {
        return this.globalSalary;
    }

    public int getSalaryDayDate() {
        return this.date;
    }

    public void setSalaryDayDate(int date) {
        this.date = date;
    }

    public String getMessage(String key) {
        return this.messages.get(key);
    }

    public List<UUID> getNoSalaryUUIDs() {
        return this.noSalaryUUIDs;
    }
}
