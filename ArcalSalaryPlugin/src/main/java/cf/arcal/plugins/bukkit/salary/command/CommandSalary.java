package cf.arcal.plugins.bukkit.salary.command;

import cf.arcal.plugins.api.bukkit.ArcalAPIBukkit;
import cf.arcal.plugins.api.bukkit.command.ArcalCommand;
import cf.arcal.plugins.api.util.JSONColor;
import cf.arcal.plugins.api.util.JSONTextBuilder;
import cf.arcal.plugins.bukkit.salary.ArcalSalaryPlugin;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

import java.util.Calendar;

public class CommandSalary extends ArcalCommand {
    private static CommandSalary instance = null;

    public static CommandSalary getInstance() {
        if (instance == null) instance = new CommandSalary();
        return instance;
    }

    private CommandSalary() {
        super("salary", "arcalapi.cmduse.salary");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            this.showHelp(sender, args);
            return;
        }

        String subcmd = args[0];
        switch (subcmd) {
            case "now":
                this.doSalaryNow(sender);
                break;
            case "reload":
                this.doReload(sender);
                break;
            default:
                this.doUnknownAction(sender, args);
                this.showHelp(sender, args);
                break;
        }
    }

    private void doUnknownAction(CommandSender sender, String[] args) {
        ArcalAPIBukkit api = ArcalAPIBukkit.getInstance();
        api.chat().sendToSender(sender,
            JSONTextBuilder.translatable("Unknown subcommand %s.")
                .addWith(JSONTextBuilder.text("\"" + args[0] + "\"").setColor(JSONColor.WHITE))
                .setColor(JSONColor.RED)
        );
    }

    private void doSalaryNow(CommandSender sender) {
        ArcalSalaryPlugin plugin = ArcalSalaryPlugin.getInstance();
        plugin.salaryMappingConfig().setSalaryDayDate(32); // That will do the trick.

        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            plugin.salaryMappingConfig().setSalaryDayDate(Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
            plugin.getApi().chat().sendToSender(sender, JSONTextBuilder.text("Forced the salary system date to the current date...").setColor(JSONColor.YELLOW));
            plugin.getApi().chat().sendToSender(sender, JSONTextBuilder.text("You will need to do a API reload to reset to the normal state.").setColor(JSONColor.YELLOW));
        }, 2);
    }

    private void doReload(CommandSender sender) {
        ArcalAPIBukkit api = ArcalAPIBukkit.getInstance();
        if (sender.hasPermission(this.getPermission() + ".reload")) {
            api.chat().sendToSender(sender, JSONTextBuilder.text("Reloading configs...").setColor(JSONColor.GREEN));
            ArcalSalaryPlugin.getInstance().salaryMappingConfig().loadConfig();
        } else {
            api.chat().sendToSender(sender, JSONTextBuilder.text("You don't have permission to do this.").setColor(JSONColor.RED));
        }
    }


    private void showHelp(CommandSender sender, String[] args) {
        ArcalAPIBukkit api = ArcalAPIBukkit.getInstance();
        api.chat().sendToSender(sender, JSONTextBuilder.text("Usage:"));

        String format = "&6  %s &f- %s";
        if (sender.hasPermission("arcalapi.cmdsee.salary.now")) {
            api.chat().sendToSender(sender,
                JSONTextBuilder.translatable(format)
                    .addWith(JSONTextBuilder.text("/" + this.getName() + " now").setColor(JSONColor.GOLD))
                    .addWith(JSONTextBuilder.text("Gives salary right now."))
                    .alternativeColorCode('&').setColor(JSONColor.WHITE)
            );
        }
        if (sender.hasPermission("arcalapi.cmdsee.salary.reload")) {
            api.chat().sendToSender(sender,
                JSONTextBuilder.translatable(format)
                    .addWith(JSONTextBuilder.text("/" + this.getName() + " reload").setColor(JSONColor.GOLD))
                    .addWith(JSONTextBuilder.text("Reloads the configuration."))
                    .alternativeColorCode('&').setColor(JSONColor.WHITE)
            );
        }
    }

    @Override
    public boolean execute(CommandSender cs, String string, String[] strings) {
        this.execute(cs, strings);
        return true;
    }
}
