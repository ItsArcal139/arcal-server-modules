package cf.arcal.plugins.bukkit.salary;

import cf.arcal.plugins.bukkit.salary.command.CommandSalary;
import cf.arcal.plugins.bukkit.salary.config.SalaryMappingConfig;
import cf.arcal.plugins.api.util.JSONColor;
import cf.arcal.plugins.api.util.JSONTextComponent;
import cf.arcal.plugins.api.util.JSONTranslatableComponent;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import cf.arcal.plugins.api.bukkit.*;
import org.bukkit.scheduler.BukkitTask;

import java.lang.reflect.InvocationTargetException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ArcalSalaryPlugin extends JavaPlugin implements Listener {
    private static ArcalSalaryPlugin instance;
    private ArcalAPIBukkit api;
    private HashMap<Player, Integer> playerJoinTickMap = new HashMap<>();
    private SalaryMappingConfig salaryMappingConfig = null;
    private Map<UUID, Boolean> salaryMap = new HashMap<>();

    @Override
    public void onEnable() {
        instance = this;
        Plugin arcalApi = Bukkit.getPluginManager().getPlugin("ArcalAPI");
        if (arcalApi == null) {
            Bukkit.getConsoleSender().sendMessage(
                ChatColor.translateAlternateColorCodes('&', "&cArcalAPIBukkit not found! Disabling the plugin...")
            );
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }
        this.api = (ArcalAPIBukkit) arcalApi;
        api.registerCommand(this, CommandSalary.getInstance());

        Bukkit.getPluginManager().registerEvents(this, this);

        this.salaryMappingConfig = new SalaryMappingConfig(this);
        salaryMappingConfig.loadConfig();

        BukkitTask timerTask = Bukkit.getScheduler().runTaskTimer(this, () -> {
            for (Player p : Bukkit.getOnlinePlayers()) {
                if (playerJoinTickMap.keySet().contains(p)) {
                    playerJoinTickMap.put(p, Math.min(100, playerJoinTickMap.get(p) + 1));
                } else {
                    playerJoinTickMap.put(p, 0);
                }
            }

            if (Bukkit.getPluginManager().getPlugin("Vault") != null) {
                if (Calendar.getInstance().get(Calendar.DATE) == salaryMappingConfig.getSalaryDayDate()) {
                    HashMap<UUID, Double> map = salaryMappingConfig.getMapping();

                    for (Player p : Bukkit.getOnlinePlayers()) {
                        if (!this.salaryMap.keySet().contains(p.getUniqueId()) && playerJoinTickMap.get(p) >= 10) {
                            double salary = salaryMappingConfig.getGlobalSalary();
                            if (map.keySet().contains(p.getUniqueId())) {
                                salary = map.get(p.getUniqueId());
                            }

                            if (this.salaryMappingConfig.getNoSalaryUUIDs().contains(p.getUniqueId()) && salary >= 0) {
                                salary = 0;
                            }

                            // Our reflection part!
                            try {
                                boolean success = api.applyPlayerMoney(p, salary);
                                if (success) {
                                    if (salary > 0) {
                                        api.chat().sendToPlayer(
                                            p, new JSONTranslatableComponent(
                                                salaryMappingConfig.getMessage("earned")
                                            ).addWith(new JSONTextComponent(String.format("$%.2f", salary)))
                                                .alternativeColorCode('&')
                                                .setColor(JSONColor.GREEN)
                                        );
                                    } else if (salary == 0) {
                                        api.chat().sendToPlayer(
                                            p, new JSONTranslatableComponent(
                                                salaryMappingConfig.getMessage("no-salary")
                                            )
                                                .alternativeColorCode('&')
                                                .setColor(JSONColor.RED)
                                        );
                                    } else {
                                        api.chat().sendToPlayer(
                                            p, new JSONTranslatableComponent(
                                                salaryMappingConfig.getMessage("negative")
                                            ).addWith(new JSONTextComponent(String.format("$%.2f", -salary)))
                                                .alternativeColorCode('&')
                                                .setColor(JSONColor.RED)
                                        );
                                    }
                                    this.salaryMap.put(p.getUniqueId(), true);
                                }
                            } catch (ClassNotFoundException | NoSuchMethodException | InvocationTargetException | IllegalAccessException ex) {
                                // Failed. lol
                                ex.printStackTrace();
                                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), String.format("eco give %s %.2f", p.getName(), salary));
                                this.salaryMap.put(p.getUniqueId(), true);
                            } catch (IllegalStateException ex) {
                                api.chat().log(new JSONTextComponent(ex.toString()).setColor(JSONColor.RED));
                            }
                        }
                    }
                } else {
                    if (this.salaryMap.keySet().size() > 0) {
                        this.salaryMap.clear();
                    }
                }
            }
        }, 1, 0);
    }

    @Override
    public void onDisable() {
        instance = null;
    }

    @EventHandler
    @SuppressWarnings("unused")
    public void onPlayerLeave(PlayerQuitEvent event) {
        Player p = event.getPlayer();
        if (playerJoinTickMap.keySet().contains(p)) {
            playerJoinTickMap.remove(p);
        }
    }

    public SalaryMappingConfig salaryMappingConfig() {
        return this.salaryMappingConfig;
    }

    public static ArcalSalaryPlugin getInstance() {
        return instance;
    }

    public ArcalAPIBukkit getApi() {
        return this.api;
    }
}
