package cf.arcal.plugins.api.util;

public class JSONKeybindComponent extends JSONComponent {
    private String keybind;

    public JSONKeybindComponent(String keybind) {
        this.keybind = keybind;
    }

    public String getKeybind() {
        return this.keybind;
    }

    public void setKeybind(String keybind) {
        if (keybind == null) {
            throw new IllegalArgumentException("Keybind must not be null.");
        }
        this.keybind = keybind;
    }

    @Override
    public JSONComponent clone() {
        JSONKeybindComponent cloned = (JSONKeybindComponent) super.clone();
        cloned.setKeybind(this.keybind);
        return cloned;
    }

    @Override
    public JSONComponent getEmptyComponent() {
        return new JSONKeybindComponent("");
    }
}
