package cf.arcal.plugins.api.util;

/**
 * The player provider base class. This class cannot be inherited.
 *
 * @param <T> The class of the platform player.
 */
public abstract class PlayerProvider<T> {
    /**
     * Send a message to the player.
     *
     * @param msg The desired message.
     */
    public abstract void sendMessage(String msg);

    /**
     * Get the associated player.
     *
     * @return The associated player.
     */
    public abstract T getPlayer();
}
