package cf.arcal.plugins.api.util;

import java.util.*;

public class JSONTranslatableComponent extends JSONComponent {
    private String translate;
    private List<JSONComponent> with = new ArrayList<>();

    public JSONTranslatableComponent(String translate) {
        this.translate = translate;
    }

    public String getTranslate() {
        return this.translate;
    }

    public JSONTranslatableComponent setTranslate(String translate) {
        if (translate == null) {
            throw new IllegalArgumentException("Translate must not be null.");
        }
        this.translate = translate;
        return this;
    }

    public List<JSONComponent> getWiths() {
        return this.with;
    }

    public JSONTranslatableComponent setWiths(List<JSONComponent> with) {
        this.with = with;
        return this;
    }

    public JSONTranslatableComponent addWith(JSONComponent comp) {
        this.with.add(comp);
        return this;
    }

    @Override
    public JSONComponent clone() {
        JSONTranslatableComponent cloned = (JSONTranslatableComponent) super.clone();
        cloned.setTranslate(this.translate);
        for (JSONComponent item : this.with) {
            cloned.addWith(item.clone());
        }
        return cloned;
    }

    @Override
    public JSONComponent getEmptyComponent() {
        return new JSONTranslatableComponent("");
    }

    public JSONComponent alternativeColorCode(char colorChar) {
        this.translate = this.translate.replace(colorChar, (char) 167);
        return this;
    }
}
