package cf.arcal.plugins.api.util;

/**
 * @author Arcal
 */
public class ClassValidator {
    public static boolean isClassLoaded(String name) {
        try {
            Class c = Class.forName(name);
            return true;
        } catch (ClassNotFoundException ex) {
            return false;
        }
    }
}
