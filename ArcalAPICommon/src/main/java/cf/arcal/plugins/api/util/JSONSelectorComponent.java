package cf.arcal.plugins.api.util;

public class JSONSelectorComponent extends JSONComponent {
    private String selector;

    public JSONSelectorComponent(String selector) {
        this.selector = selector;
    }

    public void setSelector(String selector) {
        if (selector == null) {
            throw new IllegalArgumentException("Selector must not be null.");
        }
        this.selector = selector;
    }

    public String getSelector() {
        return this.selector;
    }

    @Override
    public JSONComponent clone() {
        JSONSelectorComponent cloned = (JSONSelectorComponent) super.clone();
        cloned.setSelector(this.selector);
        return cloned;
    }

    @Override
    public JSONComponent getEmptyComponent() {
        return new JSONSelectorComponent("@s");
    }
}
