package cf.arcal.plugins.api.config;


import java.io.*;

import cf.arcal.plugins.api.*;

/**
 * The wrapper class of the platform configuration class.
 *
 * @author Arcal
 */
public abstract class ConfigProvider<T> {
    private T config;

    /**
     * Create an instance of the {@code ConfigProvider<T>}.
     *
     * @param api    The plugin platform.
     * @param config The configuration class of the platform.
     */
    public ConfigProvider(ArcalAPIPlatform api, T config) {
        this.config = config;
    }

    /**
     * Load the configuration from a desired file.
     *
     * @param file The configuration file. Usually an YAML file.
     * @return The configuration instance of the platform.
     * @throws IOException if errors occurred during the process.
     */
    public abstract T load(File file) throws IOException;

    /**
     * Set the configuration instance of the wrapper.
     *
     * @param config The desired configuration instance.
     */
    public void setConfig(T config) {
        this.config = config;
    }

    /**
     * Get the configuration instance associated to this wrapper.
     *
     * @return The associated configuration.
     */
    public T getConfig() {
        return config;
    }

    public abstract String getString(String name);

    public abstract void setString(String name, String value);

    public Class<T> getConfigClass() {
        return (Class<T>) config.getClass();
    }
}
