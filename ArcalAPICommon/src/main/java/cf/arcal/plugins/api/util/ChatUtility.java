package cf.arcal.plugins.api.util;

/**
 * @author Arcal
 */
public abstract class ChatUtility {
    // private String prefix = "&f";
    private JSONComponent prefix = new JSONTextComponent(" ")
        .setColor(JSONColor.WHITE).addExtra(
            new JSONTranslatableComponent("| %s | ")
                .addWith(
                    new JSONTextComponent("Arcal")
                        .setColor(JSONColor.GOLD)
                        .addExtra(
                            new JSONTextComponent("Network")
                                .setColor(JSONColor.WHITE)
                        )
                ).setColor(JSONColor.GRAY)
        );

    /**
     * Get the chat prefix used by the API.
     *
     * @return The prefix used by the API.
     */
    public JSONComponent getPrefix() {
        return this.prefix.clone();
    }

    /**
     * Set the chat prefix to be used.
     *
     * @param prefix The prefix to be used.
     */
    public void setPrefix(JSONComponent prefix) {
        this.prefix = prefix;
    }

    /**
     * Send chat messages to the specified player.
     *
     * @param provider  The provider wrapper of the player.
     * @param component The desired message component.
     */
    public abstract void sendToPlayer(PlayerProvider provider, JSONComponent component);

    /**
     * Broadcast messages to all the players on the server.
     *
     * @param component The desired message component.
     */
    public abstract void broadcast(JSONComponent component);

    /**
     * Log messages with API prefix.
     *
     * @param component The desired message component.
     */
    public abstract void log(JSONComponent component);
}
