package cf.arcal.plugins.api.util;

public class JSONTextComponent extends JSONComponent {
    private String text;

    public JSONTextComponent(String text) {
        this.text = text;
    }

    public JSONTextComponent() {
        this("");
    }

    public void setText(String text) {
        if (text == null) {
            throw new IllegalArgumentException("Text must not be null.");
        }
        this.text = text;
    }

    public String getText() {
        return this.text;
    }

    @Override
    public JSONComponent clone() {
        JSONTextComponent cloned = (JSONTextComponent) super.clone();
        cloned.setText(this.text);
        return cloned;
    }

    @Override
    public JSONComponent getEmptyComponent() {
        return new JSONTextComponent();
    }

    public JSONComponent alternativeColorCode(char colorChar) {
        this.text = this.text.replace(colorChar, (char) 167);
        return this;
    }
}
