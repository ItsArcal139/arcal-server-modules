package cf.arcal.plugins.api.util;

public class JSONScoreComponent extends JSONComponent {
    public static class Score implements Cloneable {
        private String name;
        private String objective;

        public Score(String name, String objective) {
            this.name = name;
            this.objective = objective;
            this.testNull();
        }

        public String getName() {
            return this.name;
        }

        public String getObjective() {
            return this.objective;
        }

        public void setName(String name) {
            if (name == null) {
                throw new IllegalArgumentException("Name must not be null.");
            }
            this.name = name;
        }

        public void setObjective(String objective) {
            if (objective == null) {
                throw new IllegalArgumentException("Objective must not be null.");
            }
            this.objective = objective;
        }

        public void testNull() {
            if (this.name == null) {
                throw new IllegalArgumentException("Name must not be null.");
            }
            if (this.objective == null) {
                throw new IllegalArgumentException("Objective must not be null.");
            }
        }

        @Override
        public Score clone() {
            return new Score(this.getName(), this.getObjective());
        }
    }

    private Score score;

    public JSONScoreComponent(Score score) {
        this.score = score;
    }

    public Score getScore() {
        return this.score;
    }

    public void setScore(Score score) {
        if (score == null) {
            throw new IllegalArgumentException("Score must not be null.");
        }
        score.testNull();
        this.score = score;
    }

    @Override
    public JSONComponent clone() {
        JSONScoreComponent cloned = (JSONScoreComponent) super.clone();
        cloned.setScore(this.score.clone());
        return cloned;
    }

    @Override
    public JSONComponent getEmptyComponent() {
        return new JSONScoreComponent(new JSONScoreComponent.Score("", ""));
    }

}
