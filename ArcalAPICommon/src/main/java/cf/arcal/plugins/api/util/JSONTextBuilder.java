package cf.arcal.plugins.api.util;

public class JSONTextBuilder {
    public static JSONTextComponent text(String text) {
        return new JSONTextComponent(text);
    }

    public static JSONTranslatableComponent translatable(String text) {
        return new JSONTranslatableComponent(text);
    }
}
