package cf.arcal.plugins.api.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectUtil {
    public static class ResultWrapper<T> {
        private T result;

        public ResultWrapper(T result) {
            this.result = result;
        }

        public T getResult() {
            return this.result;
        }

        public void setResult(T result) {
            this.result = result;
        }
    }

    public static <T> T invoke(Object thisObj, Method method, Class<T> returnType, Object... params) throws InvocationTargetException, IllegalAccessException {
        return (T) method.invoke(thisObj, params);
    }

    public static <T> boolean tryInvoke(ResultWrapper<T> resultWrapper, Object thisObj, Method method, Class<T> returnType, Object... params) {
        try {
            T result = ReflectUtil.invoke(thisObj, method, returnType, params);
            resultWrapper.setResult(result);
            return true;
        } catch (IllegalAccessException | InvocationTargetException ex) {
            return false;
        }
    }

    /**
     * Get a field of an object, even if the field is private.
     *
     * @param thisObj   The owner of the field.
     * @param name      The name of the field.
     * @param fieldType The class of the field.
     * @param <T>       The type of the field.
     * @return The value of the field.
     * @throws NoSuchFieldException   Throws when the field is not found.
     * @throws IllegalAccessException Throws when the access is considered illegal.
     */
    public static <T> T getField(Object thisObj, String name, Class<T> fieldType) throws NoSuchFieldException, IllegalAccessException {
        Field field = thisObj.getClass().getField(name);
        boolean isAccessible = field.isAccessible();

        field.setAccessible(true);
        T result = (T) field.get(thisObj);
        field.setAccessible(isAccessible);
        return result;
    }

    /**
     * Set a field for an object, even if the field is private.
     *
     * @param thisObj The owner of the field.
     * @param name    The name of the field.
     * @param value   The given value for the field.
     * @param <T>     The type of the field.
     * @throws NoSuchFieldException   Throws when the field is not found.
     * @throws IllegalAccessException Throws when the access is considered illegal.
     */
    public static <T> void setField(Object thisObj, String name, T value) throws NoSuchFieldException, IllegalAccessException {
        Field field = thisObj.getClass().getField(name);
        boolean isAccessible = field.isAccessible();

        field.setAccessible(true);
        field.set(thisObj, value);
        field.setAccessible(isAccessible);
    }
}
