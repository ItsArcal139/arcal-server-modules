package cf.arcal.plugins.api.util;

import java.net.URL;
import java.util.*;

public abstract class JSONComponent implements Cloneable {
    public static class HoverEvent implements Cloneable {
        private HoverAction action;
        private JSONComponent value;

        public HoverEvent(HoverAction action, JSONComponent value) {
            this.action = action;
            this.value = value;
        }

        public HoverAction getAction() {
            return this.action;
        }

        public JSONComponent getValue() {
            return this.value;
        }

        public void setAction(HoverAction action) {
            this.action = action;
        }

        public void setValue(JSONComponent value) {
            this.value = value;
        }

        @Override
        public HoverEvent clone() {
            return new HoverEvent(this.action, this.value);
        }

        public static HoverEvent showEntity(String name, String type, UUID uuid) {
            String entityData = "{name:\"{\\\"text\\\":\\\"" + name + "\\\"}\",type:\"" + type + "\",id:\"" + uuid.toString() + "\"}";
            return new HoverEvent(HoverAction.SHOW_ENTITY, new JSONTextComponent(entityData));
        }

        public static HoverEvent showText(JSONComponent comp) {
            return new HoverEvent(HoverAction.SHOW_TEXT, comp);
        }

        public static HoverEvent showItem(String nbtstr) {
            return new HoverEvent(HoverAction.SHOW_ITEM, JSONTextBuilder.text(nbtstr));
        }
    }

    public static class ClickEvent implements Cloneable {
        private ClickAction action;
        private String value;

        public ClickEvent(ClickAction action, String value) {
            this.action = action;
            this.value = value;
        }

        public ClickAction getAction() {
            return this.action;
        }

        public String getValue() {
            return this.value;
        }

        public void setAction(ClickAction action) {
            this.action = action;
        }

        public void setValue(String value) {
            this.value = value;
        }

        @Override
        public ClickEvent clone() {
            return new ClickEvent(this.action, this.value);
        }

        public static ClickEvent runCommand(String command) {
            return new ClickEvent(ClickAction.RUN_COMMAND, command);
        }

        public static ClickEvent suggestCommand(String command) {
            return new ClickEvent(ClickAction.SUGGEST_COMMAND, command);
        }

        public static ClickEvent openUrl(String url) {
            return new ClickEvent(ClickAction.OPEN_URL, url);
        }
    }

    public enum HoverAction {
        SHOW_TEXT,
        SHOW_ENTITY,
        SHOW_ITEM,
        SHOW_ACHIEVEMENT
    }

    public enum ClickAction {
        OPEN_URL,
        OPEN_FILE,
        RUN_COMMAND,
        SUGGEST_COMMAND,
        CHANGE_PAGE
    }

    protected JSONComponent() {
    }

    private boolean bold = false;
    private boolean italic = false;
    private boolean obfuscated = false;
    private boolean strikethrough = false;
    private boolean underlined = false;

    private JSONColor color = null;

    private HoverEvent hoverEvent = null;
    private ClickEvent clickEvent = null;

    private List<JSONComponent> extras = new ArrayList<>();
    private String insertion = null;

    private JSONComponent parent = null;

    public JSONComponent setBold(boolean bold) {
        this.bold = bold;
        return this;
    }

    @Override
    public JSONComponent clone() {
        JSONComponent cloned = this.getEmptyComponent();
        for (JSONComponent extra : this.extras) {
            cloned.addExtra(extra.clone());
        }
        cloned.setBold(this.bold);
        cloned.setColor(this.color);
        cloned.setItalic(this.italic);
        cloned.setObfuscated(this.obfuscated);
        cloned.setInsertion(this.insertion);

        if (this.clickEvent != null) {
            cloned.setClickEvent(this.clickEvent.clone());
        }

        if (this.hoverEvent != null) {
            cloned.setHoverEvent(this.hoverEvent.clone());
        }

        return cloned;
    }

    protected abstract JSONComponent getEmptyComponent();

    public boolean isBold() {
        return this.bold;
    }

    public JSONComponent setItalic(boolean italic) {
        this.italic = italic;
        return this;
    }

    public boolean isItalic() {
        return this.italic;
    }

    public JSONComponent setObfuscated(boolean obfuscated) {
        this.obfuscated = obfuscated;
        return this;
    }

    public boolean isObfuscated() {
        return this.obfuscated;
    }

    public JSONColor getColor() {
        return this.color;
    }

    public JSONComponent setColor(JSONColor color) {
        this.color = color;
        return this;
    }

    public HoverEvent getHoverEvent() {
        return this.hoverEvent;
    }

    public ClickEvent getClickEvent() {
        return this.clickEvent;
    }

    public boolean isStrikethrough() {
        return this.strikethrough;
    }

    public boolean isUnderlined() {
        return this.underlined;
    }

    public JSONComponent setStrikethrough(boolean strikethrough) {
        this.strikethrough = strikethrough;
        return this;
    }

    public JSONComponent setUnderlined(boolean underlined) {
        this.underlined = underlined;
        return this;
    }

    public JSONComponent setHoverEvent(HoverEvent event) {
        this.hoverEvent = event;
        return this;
    }

    public JSONComponent setClickEvent(ClickEvent event) {
        this.clickEvent = event;
        return this;
    }

    public JSONComponent setExtras(List<JSONComponent> extras) {
        this.extras = extras;
        return this;
    }

    public List<JSONComponent> getExtras() {
        return this.extras;
    }

    public JSONComponent addExtra(JSONComponent text) {
        this.extras.add(text);
        return this;
    }

    protected JSONComponent setParent(JSONComponent comp) {
        this.parent = comp;
        return this;
    }

    public String getInsertion() {
        return this.insertion;
    }

    public JSONComponent setInsertion(String insertion) {
        this.insertion = insertion;
        return this;
    }

    public JSONComponent getParent() {
        return this.parent;
    }

    public JSONComponent getColorlessClone() {
        JSONComponent cloned = this.clone();
        cloned.setColor(JSONColor.RESET);
        cloned.setExtras(new ArrayList<>());

        for (JSONComponent extra : this.extras) {
            cloned.addExtra(extra.getColorlessClone());
        }
        return cloned;
    }
}
