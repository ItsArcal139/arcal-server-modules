package cf.arcal.plugins.api.util;

import java.net.*;
import java.io.*;

/**
 * @author Arcal
 */
public class JarLoader {
    public static Class classFromJar(File file, String className) {
        try {
            URLClassLoader cl = URLClassLoader.newInstance(new URL[]{file.toURI().toURL()});
            return cl.loadClass(className);
        } catch (MalformedURLException | ClassNotFoundException ex) {
            // ;
        }
        return null;
    }
}
