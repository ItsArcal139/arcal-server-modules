package cf.arcal.plugins.api.exceptions;

public class ArcalAPIException extends RuntimeException {
    public ArcalAPIException() {
        super();
    }

    public ArcalAPIException(String message) {
        super(message);
    }

    public ArcalAPIException(String message, Throwable t) {
        super(message, t);
    }
}
