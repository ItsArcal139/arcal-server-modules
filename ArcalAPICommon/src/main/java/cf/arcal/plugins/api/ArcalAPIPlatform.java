/*
 * @author Arcal
 */
package cf.arcal.plugins.api;

import java.io.*;

import cf.arcal.plugins.api.util.*;
import cf.arcal.plugins.api.config.*;

/**
 * @author Arcal
 */
public interface ArcalAPIPlatform {
    File getDataFolder();

    ChatUtility chat();

    <T extends BaseAPIConfig> T config();

    InputStream getResourceAsStream(String resource);

    // Custom API
    <T> ConfigProvider<T> getConfigProvider();

    <T> ConfigProvider<T> createConfigProviderByLoadingFile(File file) throws IOException;

    default ConfigProvider createConfigProvider(File file) {
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            return this.createConfigProviderByLoadingFile(file);
        } catch (IOException ex) {
            return null;
        }
    }

    void reload();

    int getOnlinePlayerCount();
    int getMaxPlayers();
}
