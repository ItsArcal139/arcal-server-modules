package cf.arcal.plugins.api.net;

import java.net.*;

public class ErrorReporter {
    private static ErrorReporter instance = null;

    public ErrorReporter getInstance() {
        if (instance == null) {
            instance = new ErrorReporter();
        }
        return instance;
    }

    private WebSocketClient client = null;

    private ErrorReporter() {
        this.connectIfNot();
    }

    private void connectIfNot() {
        if (this.client == null) {
            final String uriStr = "ws://www.arcal.cf:9487/report/MurderMystery";
            try {
                this.client = new WebSocketClient(new URI(uriStr));
            } catch (URISyntaxException ex) {
                System.err.println("URI syntax error => " + uriStr);
            }
        }
    }

    public boolean report(Exception ex) {
        try {
            this.connectIfNot();
            client.send("{\"exception\":\"\"}");
        } catch (Exception x) {
            x.printStackTrace();
            return false;
        }
        return true;
    }
}
