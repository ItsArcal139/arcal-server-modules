/*
 * @author Arcal
 */
package cf.arcal.plugins.api.config;

import java.io.*;
import java.nio.file.*;

import cf.arcal.plugins.api.*;
import cf.arcal.plugins.api.util.JSONColor;
import cf.arcal.plugins.api.util.JSONTextComponent;

/**
 * @param <T> The class of the configuration instance.
 * @author Arcal
 */
public abstract class BaseAPIConfig<T> {

    /**
     * The associated configuration instance.
     */
    protected T config = null;

    /**
     * Get the associated platform API instance.
     *
     * @return The API instance.
     */
    public abstract ArcalAPIPlatform getAPI();

    /**
     * Load the configuration from the file, using the default way.
     *
     * @return true if the configuration has been loaded successfully.
     */
    public boolean loadConfig() {
        ArcalAPIPlatform api = this.getAPI();
        if (!api.getDataFolder().exists()) {
            api.chat().log(new JSONTextComponent("Data folder doesn't exist. Creating one..."));
            api.getDataFolder().mkdir();
        }

        File configFile = new File(api.getDataFolder(), "config.yml");

        if (!configFile.exists()) {
            try (InputStream in = api.getResourceAsStream("config.yml")) {
                api.chat().log(new JSONTextComponent("Copying the default config to the disk..."));
                Files.copy(in, configFile.toPath());
            } catch (IOException ex) {
                api.chat().log(new JSONTextComponent("Failed to copy default config onto the disk!").setColor(JSONColor.RED));
                ex.printStackTrace();
            }
        }

        /*
        this.provider = ConfigurationProvider.getProvider(YamlConfiguration.class);
        try {
            this.config = this.provider.load(this.configFile);
        } catch(IOException ex) {
            api.chat().log("&cFailed to load config from the disk!");
            ex.printStackTrace();
        }
        */

        ConfigProvider provider = api.getConfigProvider();
        try {
            this.config = (T) provider.load(configFile);
        } catch (IOException ex) {
            api.chat().log(new JSONTextComponent("Failed to load config from the disk!").setColor(JSONColor.RED));
            ex.printStackTrace();
        }

        return true;
    }

    /**
     * Save the configuration.
     */
    public abstract void saveConfig();
}
