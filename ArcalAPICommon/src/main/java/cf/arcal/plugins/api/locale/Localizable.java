package cf.arcal.plugins.api.locale;

import cf.arcal.plugins.api.ArcalAPIPlatform;
import cf.arcal.plugins.api.config.ConfigProvider;

import java.io.*;

public class Localizable {
    final String baseLocaleName = "en_us";
    final String localeName;

    File baseFile;
    File file;

    ArcalAPIPlatform mainPlugin = null;

    public Localizable(String locale, ArcalAPIPlatform plugin) {
        localeName = locale;

        this.mainPlugin = plugin;

        File langDir = new File(mainPlugin.getDataFolder(), "lang");
        if (!langDir.exists() || (langDir.exists() && !langDir.isDirectory())) {
            langDir.mkdirs();
        }

        baseFile = new File(langDir, baseLocaleName + ".yml");
        file = new File(langDir, localeName + ".yml");

        try {
            // IO related.
            baseFile.createNewFile();
            file.createNewFile();

            InputStream localeStream = mainPlugin.getResourceAsStream("lang/" + localeName + ".yml");
            if (localeStream != null) {
                // Copy to the lang folder.
                StringBuilder read = new StringBuilder();
                try {
                    int i = localeStream.read();
                    while (i != -1) {
                        read.append(String.valueOf((char) i));
                        i = localeStream.read();
                    }

                    FileOutputStream fos = new FileOutputStream(file);
                    char[] arr = read.toString().toCharArray();
                    for (char c : arr) {
                        fos.write(c);
                    }
                    fos.close();

                } catch (FileNotFoundException ex) {
                    ex.printStackTrace();
                }
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public String get(String key, Object... param) {
        return this.getByArrayParam(key, param);
    }

    public String getByArrayParam(String key, Object[] param) {
        ConfigProvider lang = mainPlugin.createConfigProvider(file);
        ConfigProvider baseLang = mainPlugin.createConfigProvider(baseFile);

        try {
            if (lang.getString(key) != null) {
                return String.format(lang.getString(key), param).replace('&', '\u00a7');
            } else if (baseLang.getString(key) != null) {
                return String.format(baseLang.getString(key), param).replace('&', '\u00a7');
            } else {
                return key;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return key;
        }
    }
}
