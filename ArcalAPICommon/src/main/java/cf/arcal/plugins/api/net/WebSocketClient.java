package cf.arcal.plugins.api.net;

import javax.websocket.*;
import java.net.*;

@ClientEndpoint
public class WebSocketClient {
    private Session s = null;
    private MessageHandler handler = null;

    public WebSocketClient(URI uri) {
        try {
            WebSocketContainer c = ContainerProvider.getWebSocketContainer();
            c.connectToServer(this, uri);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @OnOpen
    public void onOpen(Session s) {
        this.s = s;
    }

    @OnClose
    public void onClose(Session s, CloseReason reason) {
        this.s = null;
    }

    @OnMessage
    public void onMessage(String msg) {
        if (this.handler != null) {
            this.handler.handle(msg);
        }
    }

    public void setMessageHandler(MessageHandler handler) {
        this.handler = handler;
    }

    public void send(String text) {
        this.s.getAsyncRemote().sendText(text);
    }

    public interface MessageHandler {
        void handle(String msg);
    }
}
