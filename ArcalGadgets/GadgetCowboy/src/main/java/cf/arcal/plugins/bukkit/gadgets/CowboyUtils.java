package cf.arcal.plugins.bukkit.gadgets;

import org.bukkit.entity.Entity;

public class CowboyUtils {
    public static Entity getTopPassenger(Entity vehicle) {
        if(vehicle.getPassengers().size() > 0) {
            return CowboyUtils.getTopPassenger(vehicle.getPassengers().get(0));
        }
        return vehicle;
    }

    public static Entity getRootVehicle(Entity passenger) {
        if(passenger.getVehicle() != null) {
            return CowboyUtils.getRootVehicle(passenger.getVehicle());
        }
        return passenger;
    }
}
