package cf.arcal.plugins.bukkit.gadgets;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class GadgetEjector extends ArcalGadget {
    public GadgetEjector() {
        super("&dCowboy Ejector", Material.STICK);
        super.setEnchanted(true);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        if(!this.checkItem(event.getPlayer().getEquipment().getItemInMainHand())) return;
        event.setCancelled(true);
        event.getPlayer().eject();
    }
}
