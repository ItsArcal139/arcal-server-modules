package cf.arcal.plugins.bukkit.gadgets;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEntityEvent;

public class GadgetCowboy extends ArcalGadget {
    public GadgetCowboy() {
        super("&aCowboy Gadget", Material.LEAD);
    }

    @EventHandler
    public void onInteract(PlayerInteractEntityEvent event) {
        if(!this.checkItem(event.getPlayer().getEquipment().getItemInMainHand())) return;

        event.setCancelled(true);
        Entity e = event.getRightClicked();
        CowboyUtils.getTopPassenger(e).addPassenger(CowboyUtils.getRootVehicle(event.getPlayer()));
    }
}
