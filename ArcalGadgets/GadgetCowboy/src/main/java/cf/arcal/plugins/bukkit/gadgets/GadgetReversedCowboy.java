package cf.arcal.plugins.bukkit.gadgets;

import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEntityEvent;

public class GadgetReversedCowboy extends ArcalGadget {
    public GadgetReversedCowboy() {
        super("&dReversed Cowboy Gadget", Material.LEAD);
        super.setEnchanted(true);
    }

    @EventHandler
    public void onInteract(PlayerInteractEntityEvent event) {
        if(!this.checkItem(event.getPlayer().getEquipment().getItemInMainHand())) return;

        event.setCancelled(true);
        Entity e = event.getRightClicked();
        Player p = event.getPlayer();

        CowboyUtils.getTopPassenger(p).addPassenger(CowboyUtils.getRootVehicle(e));
    }
}
