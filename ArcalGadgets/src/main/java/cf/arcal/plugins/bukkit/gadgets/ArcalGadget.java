package cf.arcal.plugins.bukkit.gadgets;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;

public abstract class ArcalGadget implements Listener {
    private static HashMap<String, ArcalGadget> registry = new HashMap<>();

    private String name;
    private Material material;

    protected ArcalGadget(String name, Material material) {
        this.name = name;
        this.material = material;
    }

    private boolean enchanted = false;
    protected void setEnchanted(boolean toggle) {
        this.enchanted = toggle;
    }

    public boolean isEnchanted() {
        return this.enchanted;
    }

    public String getName() {
        return this.name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    protected ItemMeta initItemMeta() {
        return Bukkit.getItemFactory().getItemMeta(this.material);
    }

    public ItemStack toItemStack() {
        ItemStack item = new ItemStack(this.material, 1);
        ItemMeta meta = this.initItemMeta();
        meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', this.name));
        meta.setUnbreakable(true);
        meta.addItemFlags(ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ENCHANTS);
        item.setItemMeta(meta);

        if(this.enchanted) {
            item.addUnsafeEnchantment(Enchantment.MENDING, 1);
        }

        return item;
    }

    public static Set<String> getRegisteredGadgetNames() {
        return registry.keySet();
    }

    public static ArcalGadget getGadgetByName(String name) {
        return registry.get(name);
    }

    public static Collection<ArcalGadget> getRegisteredGadgets() {
        return registry.values();
    }

    public static void register(String name, ArcalGadget gadgetClass) {
        registry.put(name, gadgetClass);
    }

    public static void clearRegister() {
        registry.clear();
    }

    protected boolean checkItem(ItemStack item) {
        if(item.equals(this.toItemStack())) {
            if(item.getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes('&', this.name))) {
                return true;
            }
        }
        return false;
    }
}
