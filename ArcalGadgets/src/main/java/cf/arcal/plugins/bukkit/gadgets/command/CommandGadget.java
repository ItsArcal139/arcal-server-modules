package cf.arcal.plugins.bukkit.gadgets.command;

import cf.arcal.plugins.api.bukkit.ArcalAPIBukkit;
import cf.arcal.plugins.api.bukkit.command.ArcalCommand;
import cf.arcal.plugins.api.util.JSONColor;
import cf.arcal.plugins.api.util.JSONTextBuilder;
import cf.arcal.plugins.bukkit.gadgets.ArcalGadget;
import cf.arcal.plugins.bukkit.gadgets.ArcalGadgetsPlugin;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CommandGadget extends ArcalCommand {
    private static CommandGadget instance = null;

    public static CommandGadget getInstance() {
        if (instance == null) instance = new CommandGadget();
        return instance;
    }

    private CommandGadget() {
        super("gadget", "arcalapi.cmduse.gadget");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            this.showHelp(sender, args);
            return;
        }

        String subcmd = args[0];
        switch (subcmd) {
            case "reload":
                this.doReload(sender);
                break;
            case "list":
                this.doList(sender);
                break;
            case "give":
                this.doGive(sender, args);
                break;
            default:
                this.doUnknownAction(sender, args);
                this.showHelp(sender, args);
                break;
        }
    }

    private void doGive(CommandSender sender, String[] args) {
        ArcalAPIBukkit api = ArcalAPIBukkit.getInstance();
        if(!(sender instanceof Player)) {
            api.chat().sendToSender(sender, JSONTextBuilder.text("You are not a Player!").setColor(JSONColor.RED));
            return;
        }

        if(args.length < 2) {
            this.showHelp(sender, args);
            return;
        }

        ArcalGadget gadget = ArcalGadget.getGadgetByName(args[1]);
        if(gadget == null) {
            api.chat().sendToSender(sender, JSONTextBuilder.text("Invalid gadget!").setColor(JSONColor.RED));
            return;
        }

        ItemStack item = gadget.toItemStack();
        Player p = (Player)sender;

        Item itemEntity = p.getWorld().dropItem(p.getLocation(), item);
        itemEntity.setPickupDelay(0);

        api.chat().sendToSender(sender,
            JSONTextBuilder.text("You get a ")
            .setColor(JSONColor.GREEN)
            .addExtra(JSONTextBuilder.text(ChatColor.translateAlternateColorCodes('&', gadget.getName())).setColor(JSONColor.WHITE))
            .addExtra(JSONTextBuilder.text("!"))
        );
    }

    private void doUnknownAction(CommandSender sender, String[] args) {
        ArcalAPIBukkit api = ArcalAPIBukkit.getInstance();
        api.chat().sendToSender(sender,
            JSONTextBuilder.translatable("Unknown subcommand %s.")
                .addWith(JSONTextBuilder.text("\"" + args[0] + "\"").setColor(JSONColor.WHITE))
                .setColor(JSONColor.RED)
        );
    }

    private void doList(CommandSender sender) {
        ArcalAPIBukkit api = ArcalAPIBukkit.getInstance();
        api.chat().sendToSender(sender, JSONTextBuilder.text("List of registered gadget:"));
        for(String key : ArcalGadget.getRegisteredGadgetNames()) {
            ArcalGadget gadget = ArcalGadget.getGadgetByName(key);
            api.chat().sendToSender(sender,
                JSONTextBuilder.text(key).setColor(JSONColor.GOLD)
                .addExtra(JSONTextBuilder.text(" => ").setColor(JSONColor.GRAY))
                .addExtra(JSONTextBuilder.text(ChatColor.translateAlternateColorCodes('&', gadget.getName())).setColor(JSONColor.WHITE))
            );
        }
    }

    private void doReload(CommandSender sender) {
        ArcalAPIBukkit api = ArcalAPIBukkit.getInstance();
        if (sender.hasPermission(this.getPermission() + ".reload")) {
            api.chat().sendToSender(sender, JSONTextBuilder.text("Reloading configs...").setColor(JSONColor.GREEN));
            ArcalGadgetsPlugin.getInstance().reload();
        } else {
            api.chat().sendToSender(sender, JSONTextBuilder.text("You don't have permission to do this.").setColor(JSONColor.RED));
        }
    }

    private void showHelp(CommandSender sender, String[] args) {
        ArcalAPIBukkit api = ArcalAPIBukkit.getInstance();
        api.chat().sendToSender(sender, JSONTextBuilder.text("Usage:"));

        String format = "  %s - %s";
        if (sender.hasPermission("arcalapi.cmdsee.gadget.give")) {
            api.chat().sendToSender(sender,
                JSONTextBuilder.translatable(format)
                    .addWith(JSONTextBuilder.text("/" + this.getName() + " give <gadget_id>").setColor(JSONColor.GOLD))
                    .addWith(JSONTextBuilder.text("Gives a gadget."))
                    .alternativeColorCode('&').setColor(JSONColor.WHITE)
            );
        }
        if (sender.hasPermission("arcalapi.cmdsee.gadget.reload")) {
            api.chat().sendToSender(sender,
                JSONTextBuilder.translatable(format)
                    .addWith(JSONTextBuilder.text("/" + this.getName() + " reload").setColor(JSONColor.GOLD))
                    .addWith(JSONTextBuilder.text("Reloads the configuration."))
                    .alternativeColorCode('&').setColor(JSONColor.WHITE)
            );
        }if (sender.hasPermission("arcalapi.cmdsee.gadget.list")) {
            api.chat().sendToSender(sender,
                JSONTextBuilder.translatable(format)
                    .addWith(JSONTextBuilder.text("/" + this.getName() + " list").setColor(JSONColor.GOLD))
                    .addWith(JSONTextBuilder.text("List loaded gadgets."))
                    .alternativeColorCode('&').setColor(JSONColor.WHITE)
            );
        }
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        this.execute(commandSender, strings);
        return true;
    }
}
