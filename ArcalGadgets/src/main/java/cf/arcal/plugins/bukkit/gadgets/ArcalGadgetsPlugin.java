package cf.arcal.plugins.bukkit.gadgets;

import cf.arcal.plugins.api.bukkit.ArcalAPIBukkit;
import cf.arcal.plugins.api.util.JSONColor;
import cf.arcal.plugins.api.util.JSONTextBuilder;
import cf.arcal.plugins.bukkit.gadgets.command.CommandGadget;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ArcalGadgetsPlugin extends JavaPlugin {
    private static ArcalGadgetsPlugin instance = null;

    public static ArcalGadgetsPlugin getInstance() {
        return instance;
    }

    private ArcalAPIBukkit api;

    @Override
    public void onEnable() {
        instance = this;

        Plugin arcalApi = Bukkit.getPluginManager().getPlugin("ArcalAPI");
        if (arcalApi == null) {
            Bukkit.getConsoleSender().sendMessage(
                ChatColor.translateAlternateColorCodes('&', "&cArcalAPIBukkit not found! Disabling the plugin...")
            );
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }
        this.api = (ArcalAPIBukkit) arcalApi;
        api.registerCommand(this, CommandGadget.getInstance());

        for(ArcalGadget gadget : ArcalGadget.getRegisteredGadgets()) {
            Bukkit.getPluginManager().registerEvents(gadget, this);
        }

        this.reload();
    }

    @Override
    public void onDisable() {
        instance = null;
    }

    public void reload() {
        ArcalGadget.clearRegister();
        HandlerList.unregisterAll(this);

        Bukkit.getScheduler().runTaskLater(this, () -> {
            api.chat().log(JSONTextBuilder.text("Reloading gadgets...").setColor(JSONColor.GREEN));

            File dataFolder = this.getDataFolder();
            if(!dataFolder.exists()) {
                dataFolder.mkdir();
            }

            File gadgetsFolder = new File(dataFolder, "gadgets/");
            if(!gadgetsFolder.exists()) {
                gadgetsFolder.mkdir();
            }

            try {
                File[] files = gadgetsFolder.listFiles((d, name) -> name.endsWith(".jar"));
                if(files != null && files.length > 0) {
                    for (File f : files) {
                        try(URLClassLoader cl = new URLClassLoader(new URL[]{ f.toURI().toURL() }, getClass().getClassLoader())) {
                            try(JarFile jar = new JarFile(f)) {
                                Enumeration<JarEntry> entries = jar.entries();

                                while (entries.hasMoreElements()) {
                                    JarEntry entry = entries.nextElement();
                                    if (entry.getName().endsWith(".class")) {
                                        String clsName = entry.getName().replace("/", ".").replace(".class", "");

                                        try {
                                            cl.loadClass(clsName);
                                        } catch (ClassNotFoundException ex) {
                                            // ...qaq
                                        }
                                    }
                                }
                            }

                            try (InputStream in = cl.getResourceAsStream("gadget.yml")) {
                                if(in == null) {
                                    throw new NullPointerException("The resource get is null.");
                                }
                                YamlConfiguration config = YamlConfiguration.loadConfiguration(new InputStreamReader(in));

                                for(String gadgetItems : config.getKeys(false)) {
                                    String key = config.getString(gadgetItems + ".gadget-key");
                                    if (key == null) {
                                        api.chat().log(JSONTextBuilder.text("Gadget key not defined in " + gadgetItems + ", " + f.getName()).setColor(JSONColor.RED));
                                        continue;
                                    }

                                    String mainClassStr = config.getString(gadgetItems + ".main");
                                    if (mainClassStr == null) {
                                        api.chat().log(JSONTextBuilder.text("Gadget main class not defined in " + gadgetItems + ", " + f.getName()).setColor(JSONColor.RED));
                                        continue;
                                    }

                                    Class clz = Class.forName(mainClassStr, true, cl);
                                    ArcalGadget gadget = (ArcalGadget) clz.getConstructor().newInstance();

                                    ArcalGadget.register(key, gadget);
                                    Bukkit.getPluginManager().registerEvents(gadget, ArcalGadgetsPlugin.getInstance());

                                    api.chat().log(
                                        JSONTextBuilder.text("Registered gadget from ")
                                            .addExtra(JSONTextBuilder.text(f.getName()).setColor(JSONColor.GOLD))
                                            .addExtra(JSONTextBuilder.text(", key = "))
                                            .addExtra(JSONTextBuilder.text(key).setColor(JSONColor.GOLD))
                                    );
                                }
                            } catch (IOException | NullPointerException ex) {
                                api.chat().log(JSONTextBuilder.text("Failed to read gadget.yml").setColor(JSONColor.RED));
                                ex.printStackTrace();
                            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | ClassCastException | NoSuchMethodException | InvocationTargetException ex) {
                                api.chat().log(JSONTextBuilder.text("Invalid gadget main class in " + f.getName()).setColor(JSONColor.RED));
                                ex.printStackTrace();
                            }
                        } catch (IOException ex) {
                            api.chat().log(JSONTextBuilder.text("Send the stack trace to the developer! We need your help!").setColor(JSONColor.RED));
                            ex.printStackTrace();
                        }
                    }

                    api.chat().log(JSONTextBuilder.text("Completed.").setColor(JSONColor.GREEN));
                } else {
                    api.chat().log(JSONTextBuilder.text("It is an empty folder."));
                    api.chat().log(JSONTextBuilder.text("Completed without loading gadgets.").setColor(JSONColor.GREEN));
                }
            } catch (NullPointerException ex) {
                api.chat().log(JSONTextBuilder.text("Failed to reload!").setColor(JSONColor.RED));
                ex.printStackTrace();
            }
        }, 10);
    }
}
