package cf.arcal.plugins.bukkit.gadgets;

import cf.arcal.plugins.bukkit.gadgets.ArcalGadget;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

public class GadgetMomentumBow extends ArcalGadget {
    public GadgetMomentumBow() {
        super("&dDouble Momentum Bow&r", Material.BOW);
        super.setEnchanted(true);
    }

    @EventHandler
    public void onUse(EntityShootBowEvent event) {
        if(!this.checkItem(event.getBow())) return;
        if(event.getEntity() instanceof Player) {
            Entity e = event.getProjectile();
            e.setMetadata("MomentumArrow", new FixedMetadataValue(ArcalGadgetsPlugin.getInstance(), true));
            Player p = (Player) event.getEntity();
            p.getWorld().dropItem(p.getLocation(), new ItemStack(Material.ARROW, 1)).setPickupDelay(0);
        }
    }

    @EventHandler
    public void onHit(EntityDamageByEntityEvent event) {
        Entity e = event.getDamager();
        Entity target = event.getEntity();

        if(e.hasMetadata("MomentumArrow")) {
            target.setVelocity(e.getVelocity().multiply(2));
        }
    }
}
