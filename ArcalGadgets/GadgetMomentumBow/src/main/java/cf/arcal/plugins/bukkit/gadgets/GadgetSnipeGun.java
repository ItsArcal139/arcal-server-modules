package cf.arcal.plugins.bukkit.gadgets;

import cf.arcal.plugins.api.bukkit.ArcalAPIBukkit;
import cf.arcal.plugins.api.util.JSONColor;
import cf.arcal.plugins.api.util.JSONComponent;
import cf.arcal.plugins.api.util.JSONTextBuilder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

public class GadgetSnipeGun extends ArcalGadget {
    public GadgetSnipeGun() {
        super("&b奪命之吻", Material.BOW);
        super.setEnchanted(true);
    }

    @EventHandler
    public void onUse(EntityShootBowEvent event) {
        if(event.getBow().getItemMeta() == null) return;
        if(event.getBow().getItemMeta().getDisplayName() == null) return;
        if(!event.getBow().getItemMeta().getDisplayName().contains("奪命之吻")) return;
        if(event.getEntity() instanceof Player) {
            Player p = (Player) event.getEntity();

            Arrow e = (Arrow)event.getProjectile();
            Vector v = p.getEyeLocation().getDirection(); // Hitscan
            e.setVelocity(v.multiply(500));
            e.setMetadata("ArrowOwner", new FixedMetadataValue(ArcalGadgetsPlugin.getInstance(), p));
            ArcalAPIBukkit.getInstance().chat().log(
                JSONTextBuilder.text("BowForce=" + event.getForce())
            );
            e.setGravity(false);


            if(event.getBow().getItemMeta().isUnbreakable()) {
                e.setMetadata("BowForce", new FixedMetadataValue(ArcalGadgetsPlugin.getInstance(), event.getForce() * 2));
                p.getWorld().playSound(p.getEyeLocation(), Sound.BLOCK_NOTE_BLOCK_SNARE, 0.1f, 0.3f);
                p.getWorld().dropItem(p.getLocation(), new ItemStack(Material.ARROW, 1)).setPickupDelay(0);
            } else {
                e.setMetadata("BowForce", new FixedMetadataValue(ArcalGadgetsPlugin.getInstance(), event.getForce()));
            }

            if(event.getForce() > 0.75) {
                p.getWorld().playSound(p.getEyeLocation(), Sound.ENTITY_FIREWORK_ROCKET_LARGE_BLAST_FAR, 5.0f, 1.0f);
            } else if(event.getForce() > 0.4) {
                p.getWorld().playSound(p.getEyeLocation(), Sound.ENTITY_FIREWORK_ROCKET_LARGE_BLAST, 4.0f, 1.0f);
            } else {
                p.getWorld().playSound(p.getEyeLocation(), Sound.ENTITY_FIREWORK_ROCKET_BLAST, 1.0f, 1.05f);
            }
        }
    }

    @EventHandler
    public void onLeftClick(PlayerInteractEvent event) {
        if(event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) {
            if(event.getItem().getType() != Material.BOW) return;
            if(event.getItem().getItemMeta() == null) return;
            if(event.getItem().getItemMeta().getDisplayName() == null) return;
            if(!event.getItem().getItemMeta().getDisplayName().contains("奪命之吻")) return;

            event.setCancelled(true);

            Player p = event.getPlayer();
            Vector v = p.getEyeLocation().getDirection(); // Hitscan
            Arrow e = p.launchProjectile(Arrow.class, v.multiply(500));

            float force = 0.1f;
            if(event.getItem().getItemMeta().isUnbreakable()) {
                e.setMetadata("BowForce", new FixedMetadataValue(ArcalGadgetsPlugin.getInstance(), force * 2f));
                p.getWorld().playSound(p.getEyeLocation(), Sound.BLOCK_NOTE_BLOCK_SNARE, 0.1f, 0.3f);
            } else {
                e.setMetadata("BowForce", new FixedMetadataValue(ArcalGadgetsPlugin.getInstance(), force));
                e.setPickupStatus(Arrow.PickupStatus.DISALLOWED);
            }

            e.setMetadata("ArrowOwner", new FixedMetadataValue(ArcalGadgetsPlugin.getInstance(), p));
            e.setMetadata("NotSniping", new FixedMetadataValue(ArcalGadgetsPlugin.getInstance(), true));
            e.setGravity(false);
            p.getWorld().playSound(p.getEyeLocation(), Sound.ENTITY_FIREWORK_ROCKET_BLAST_FAR, 1.0f, 1.0f);
        }
    }

    @EventHandler
    public void onHit(EntityDamageByEntityEvent event) {
        ArcalAPIBukkit api = ArcalAPIBukkit.getInstance();
        Entity e = event.getDamager();
        Entity target = event.getEntity();

        if(!e.hasMetadata("BowForce")) return;
        if(!e.hasMetadata("ArrowOwner")) return;

        Object fO = e.getMetadata("BowForce").get(0).value();
        Object aO = e.getMetadata("ArrowOwner").get(0).value();

        if(!(fO instanceof Float)) return;
        if(!(aO instanceof Player)) return;

        Player p = (Player) aO;

        boolean isHeadshot = false;
        float force = (float) fO;
        if(target instanceof LivingEntity && e instanceof Arrow) {
            Location eyeLocation = ((LivingEntity) target).getEyeLocation();
            double dist = p.getEyeLocation().distance(eyeLocation);

            Location loc = p.getEyeLocation().add(p.getEyeLocation().getDirection().multiply(dist));
            double eyeDistance = loc.distance(eyeLocation);

            loc.getWorld().playSound(loc, Sound.ENTITY_FIREWORK_ROCKET_SHOOT, 1.5f, 1.05f);

            if (eyeDistance <= 0.3) {
                force *= 2;
                isHeadshot = true;
            }
            api.chat().log(JSONTextBuilder.text("Distance to eye = ").addExtra(JSONTextBuilder.text(String.format("%.2f", eyeDistance)).setColor(JSONColor.GOLD)));
        }

        event.setDamage(12 * force);
        if(target instanceof LivingEntity) {
            double hp = ((LivingEntity) target).getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
            if(hp > 20) {
                event.setDamage((12 + hp * 0.025) * force);
            }
        }

        JSONComponent targetComp;
        if(target instanceof Player) {
            targetComp = api.getPlayerComponent((Player) target);
        } else {
            targetComp = JSONTextBuilder.translatable(target.getCustomName() == null ? "entity.minecraft." + target.getType().name().toLowerCase() : target.getCustomName());
        }

        JSONComponent comp = JSONTextBuilder.text("你打中 ").addExtra(targetComp).addExtra(JSONTextBuilder.text(" 了!"));
        if(isHeadshot) {
            comp.addExtra(JSONTextBuilder.text(" 爆頭!!").setColor(JSONColor.RED).setBold(true).setHoverEvent(
               JSONComponent.HoverEvent.showText(JSONTextBuilder.text("你打中頭了。"))
            ));

            if(((LivingEntity) target).getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue() > 20
                && ((LivingEntity) target).getHealth() - event.getDamage() <= 0) {
                api.chat().broadcast(
                    api.getPlayerComponent(p)
                        .addExtra(JSONTextBuilder.text(" 爆頭擊殺 "))
                        .addExtra(targetComp)
                        .addExtra(JSONTextBuilder.text("!"))
                );
            } else if(((LivingEntity) target).getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue() == 20) {
                api.chat().broadcast(
                    api.getPlayerComponent(p)
                        .addExtra(JSONTextBuilder.text(" 爆頭擊中 "))
                        .addExtra(targetComp)
                        .addExtra(JSONTextBuilder.text("!"))
                );
            } else {
                api.chat().sendToPlayer(p, comp);
            }
        } else {
            api.chat().sendToPlayer(p, comp);
        }
    }
}
