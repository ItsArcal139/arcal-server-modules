package cf.arcal.plugins.bukkit.stairsit.config;

import cf.arcal.plugins.api.bukkit.ArcalAPIBukkit;
import cf.arcal.plugins.api.util.JSONColor;
import cf.arcal.plugins.api.util.JSONTextBuilder;
import cf.arcal.plugins.api.util.JSONTextComponent;
import cf.arcal.plugins.bukkit.stairsit.ArcalStairSitPlugin;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.List;

public class ArcalStairSitConfig {
    private File configFile;
    private static final String FILE_NAME = "config.yml";

    private List<String> stairMaterials = null;
    private boolean stairSeatEnabled = false;
    private boolean allowSitWhenHasTopBlock = false;

    private ArcalStairSitPlugin plugin;
    private ArcalAPIBukkit api;

    public ArcalStairSitConfig(ArcalStairSitPlugin plugin) {
        this.plugin = plugin;
        this.api = plugin.getAPI();
        this.configFile = new File(plugin.getDataFolder(), FILE_NAME);
    }

    public boolean loadConfig() {
        if (!plugin.getDataFolder().exists()) {
            api.chat().log(new JSONTextComponent("Data folder doesn't exist. Creating one..."));
            plugin.getDataFolder().mkdir();
        }

        if (!this.configFile.exists()) {
            try (InputStream in = plugin.getResource(FILE_NAME)) {
                api.chat().log(JSONTextBuilder.text("Copying the default configuration to the disk...").setColor(JSONColor.GOLD));
                Files.copy(in, this.configFile.toPath());
            } catch (IOException ex) {
                api.chat().log(JSONTextBuilder.text("Failed to copy default configuration onto the disk!").setColor(JSONColor.RED));
                ex.printStackTrace();
            }
        }

        YamlConfiguration config = YamlConfiguration.loadConfiguration(this.configFile);
        this.stairMaterials = config.getStringList("stair-materials");
        this.stairSeatEnabled = config.getBoolean("stair-seat-enabled", false);
        this.allowSitWhenHasTopBlock = config.getBoolean("allow-sit-when-has-top-block", false);

        api.chat().log(JSONTextBuilder.text(plugin.getName() + " configuration loaded!").setColor(JSONColor.GREEN));
        return true;
    }

    public boolean doesAllowSitWhenHasTopBlock() {
        return this.allowSitWhenHasTopBlock;
    }

    public List<String> getStairMaterials() {
        return this.stairMaterials;
    }

    public boolean isStairSeatEnabled() {
        return this.stairSeatEnabled;
    }
}
