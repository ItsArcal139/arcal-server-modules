package cf.arcal.plugins.bukkit.stairsit;

import cf.arcal.plugins.api.bukkit.ArcalAPIBukkit;
import cf.arcal.plugins.bukkit.stairsit.config.ArcalStairSitConfig;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.material.Stairs;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class ArcalStairSitPlugin extends JavaPlugin implements Listener {
    private ArcalAPIBukkit api;
    private ArcalStairSitConfig config = null;
    private BukkitTask arrowTimerTask = null;

    @Override
    public void onEnable() {
        Plugin arcalApi = Bukkit.getPluginManager().getPlugin("ArcalAPI");
        if (arcalApi == null) {
            Bukkit.getConsoleSender().sendMessage(
                ChatColor.translateAlternateColorCodes('&', "&cArcalAPIBukkit not found! Disabling the plugin...")
            );
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }
        this.api = (ArcalAPIBukkit) arcalApi;

        this.config = new ArcalStairSitConfig(this);
        config.loadConfig();

        this.arrowTimerTask = Bukkit.getScheduler().runTaskTimer(this, () -> {
            for (Player p : Bukkit.getOnlinePlayers()) {
                World w = p.getWorld();
                for (Arrow a : w.getEntitiesByClass(Arrow.class)) {
                    String name = a.getCustomName();
                    if (name == null) continue;
                    if (name.equals("stair-vehicle")) {
                        if (a.getPassengers().isEmpty()) {
                            a.remove();
                        } else {
                            a.setTicksLived(10);
                        }
                    }
                }
            }
        }, 1, 0);
    }

    public ArcalStairSitConfig config() {
        return this.config;
    }

    public ArcalAPIBukkit getAPI() {
        return this.api;
    }

    @EventHandler
    public void onPlayerInteractStair(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            Block b = e.getClickedBlock();
            Material mat = b.getType();
            if (!b.isLiquid() && mat.isBlock()) {
                List<String> materialStrs = this.config().getStairMaterials();
                List<Material> materials = new ArrayList<>();
                for (String str : materialStrs) {
                    try {
                        Material m = Material.valueOf(str.toUpperCase());
                        materials.add(m);
                    } catch (IllegalArgumentException ex) {
                        // ;
                    }
                }

                boolean isStair = false;
                for (Material m : materials) {
                    isStair = isStair || (mat == m);
                }

                Player p = e.getPlayer();
                World w = p.getLocation().getWorld();

                boolean hasArrow = false;
                for (Arrow ar : w.getEntitiesByClass(Arrow.class)) {
                    String aName = ar.getCustomName();
                    if (aName == null) continue;
                    if (aName.equals("stair-vehicle") && ar.getLocation().getBlock().getLocation().distance(b.getLocation()) <= 0.1) {
                        hasArrow = true;
                    }
                }

                if (isStair && !hasArrow && this.config().isStairSeatEnabled()) {
                    boolean shouldCancel = false;
                    if (p.isSneaking()) shouldCancel = true;

                    Block top = b.getLocation().add(new Vector(0, 1, 0)).getBlock();
                    if (!top.isLiquid() && top.getType().isSolid()) {
                        if (!this.config().doesAllowSitWhenHasTopBlock()) shouldCancel = true;
                    }

                    if (!shouldCancel) {
                        e.setCancelled(true);
                        double modX = ((Stairs) b.getState().getData()).getFacing().getModX();
                        double modY = ((Stairs) b.getState().getData()).getFacing().getModY();
                        double modZ = ((Stairs) b.getState().getData()).getFacing().getModZ();
                        Vector modVector = new Vector(modX, modY, modZ).multiply(0.2);

                        Arrow a = w.spawnArrow(b.getLocation().add(new Vector(0.5, 0, 0.5)).add(modVector), new Vector(0, -1, 0), 0, 0);
                        a.setCustomName("stair-vehicle");
                        a.setGravity(false);
                        a.addPassenger(p);
                    }
                }
            }
        }
    }

    @Override
    public void onDisable() {
        if (this.arrowTimerTask != null) {
            this.arrowTimerTask.cancel();
        }
    }
}
