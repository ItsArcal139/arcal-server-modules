package cf.arcal.plugins.game.murder.modes;

import cf.arcal.plugins.api.bukkit.ArcalAPIBukkit;
import cf.arcal.plugins.api.game.ArcalGameAPI;
import cf.arcal.plugins.api.util.JSONColor;
import cf.arcal.plugins.api.util.JSONTextBuilder;
import cf.arcal.plugins.game.murder.data.Possibility;
import cf.arcal.plugins.game.murder.roles.Role;
import cf.arcal.plugins.game.murder.roles.RoleDetective;
import cf.arcal.plugins.game.murder.roles.RoleMurderer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ClassicMode extends MurderMysteryGameMode {
    private Possibility<Player> murdererPlayers = new Possibility<>();
    private Possibility<Player> detectivePlayers = new Possibility<>();
    private Collection<Player> players = new ArrayList<>();
    private List<Role> roles = new ArrayList<>();

    private ArcalGameAPI gameApi;
    private ArcalAPIBukkit api;

    public ClassicMode() {
        super("classic");

        this.gameApi = ArcalGameAPI.getInstance();
        this.api = this.gameApi.getApi();
    }

    @Override
    public void start() {
        Player murderer = murdererPlayers.chooseKeyByPossibilities();
        detectivePlayers.remove(murderer);
        Player detective = detectivePlayers.chooseKeyByPossibilities();

        RoleMurderer m = (RoleMurderer) Role.createByName("murderer", murderer);
        RoleDetective d = (RoleDetective) Role.createByName("detective", detective);

        Collection<Player> newPlayers = new ArrayList<>();
        newPlayers.addAll(players);
        newPlayers.remove(murderer);
        newPlayers.remove(detective);

        roles.add(m);
        roles.add(d);

        for(Player p : newPlayers) {
            roles.add(Role.createByName("innocent", p));
        }
    }

    @Override
    public void stop() {

    }

    @Override
    public void tick(int timer) {

    }

    @Override
    public int getMaxGameTime() {
        return 5 * 60 * ArcalGameAPI.TICK_TO_SEC;
    }

    @Override
    public Collection<Player> getPlayers() {
        return Collections.unmodifiableCollection(this.players);
    }

    @Override
    public void addPlayer(Player p) {
        this.players.add(p);
    }

    @Override
    public void removePlayer(Player p) {
        this.players.remove(p);
    }

    @Override
    public int getMinPlayerCount() {
        return 3;
    }

    @Override
    public void load() {
        api.chat().log(JSONTextBuilder.text("Classic mode loaded!").setColor(JSONColor.GREEN));
    }

    @Override
    public void unload() {
        api.chat().log(JSONTextBuilder.text("Classic mode unloaded!").setColor(JSONColor.GREEN));
    }
}
