package cf.arcal.plugins.game.murder.data;

import cf.arcal.plugins.game.murder.roles.Role;
import cf.arcal.plugins.game.murder.roles.RoleDetective;
import cf.arcal.plugins.game.murder.roles.RoleInnocent;
import cf.arcal.plugins.game.murder.roles.RoleMurderer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class RoleList extends ArrayList<Role> {
    public RoleMurderer getMurderer() {
        for(Role r : this) {
            if(r instanceof RoleMurderer) {
                return (RoleMurderer) r;
            }
        }
        return null;
    }

    public RoleDetective getDetective() {
        for(Role r : this) {
            if(r instanceof RoleDetective) {
                return (RoleDetective) r;
            }
        }
        return null;
    }

    public Collection<RoleInnocent> getInnocents() {
        ArrayList<RoleInnocent> roles = new ArrayList<>();
        for(Role r : this) {
            if(r instanceof RoleInnocent && !(r instanceof RoleDetective)) {
                roles.add((RoleInnocent) r);
            }
        }
        return Collections.unmodifiableCollection(roles);
    }
}
