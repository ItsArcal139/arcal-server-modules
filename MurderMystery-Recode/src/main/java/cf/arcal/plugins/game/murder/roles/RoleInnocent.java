package cf.arcal.plugins.game.murder.roles;

import org.bukkit.entity.Player;

public class RoleInnocent extends Role {
    public RoleInnocent(Player p) {
        super("innocent", p);
    }

    protected RoleInnocent(String name, Player p) {
        super(name, p);
    }
}
