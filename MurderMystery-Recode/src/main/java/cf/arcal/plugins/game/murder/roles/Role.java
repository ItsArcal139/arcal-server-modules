package cf.arcal.plugins.game.murder.roles;

import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public abstract class Role {
    private String name;
    private static Map<String, Class<? extends Role>> registry = new HashMap<>();

    public static Role createByName(String name, Player p) {
        Class<? extends Role> c = registry.get(name);
        if(c != null) {
            try {
                return c.getConstructor(Player.class).newInstance(p);
            } catch (NoSuchMethodException | InstantiationException | InvocationTargetException | IllegalAccessException ex) {
                return null;
            }
        }
        return null;
    }

    private Player player;

    protected Role(String name, Player p) {
        this.name = name;
        this.player = p;
    }

    public Player getPlayer() {
        return this.player;
    }

    public void setPlayer(Player p) {
        this.player = p;
    }

    public static void swapPlayer(Role r1, Role r2) {
        Player p1 = r1.player;
        Player p2 = r2.player;

        r1.player = p2;
        r2.player = p1;
    }

    static {
        registry.put("innocent", RoleInnocent.class);
        registry.put("detective", RoleDetective.class);
        registry.put("murderer", RoleMurderer.class);
    }
}
