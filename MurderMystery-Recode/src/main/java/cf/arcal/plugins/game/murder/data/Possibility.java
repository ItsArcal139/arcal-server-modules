package cf.arcal.plugins.game.murder.data;

import java.util.*;

public class Possibility<T> extends HashMap<T, Double> {
    public double getPercentage(T key) {
        double total = 0;
        for(double v : values()) {
            total += v;
        }

        return super.get(key) / total;
    }

    public T chooseKeyByPossibilities()  {
        double random = Math.random();

        HashMap<T, Double> deltaRange = new HashMap<>();
        for(T key : keySet()) {
            deltaRange.put(key, Math.abs(this.getPercentage(key) - random));
        }

        List<Entry<T, Double>> entries = new ArrayList<>(deltaRange.entrySet());
        entries.sort(Collections.reverseOrder(Entry.comparingByValue()));
        return entries.get(0).getKey();
    }
}
