package cf.arcal.plugins.game.murder;

import cf.arcal.plugins.api.bukkit.ArcalAPIBukkit;
import cf.arcal.plugins.api.game.ArcalGame;
import cf.arcal.plugins.api.game.ArcalGameAPI;
import cf.arcal.plugins.api.util.JSONColor;
import cf.arcal.plugins.api.util.JSONTextBuilder;
import cf.arcal.plugins.game.murder.modes.ClassicMode;
import cf.arcal.plugins.game.murder.modes.MurderMysteryGameMode;
import cf.arcal.plugins.game.murder.roles.Role;
import org.bukkit.entity.Player;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class MurderMystery implements ArcalGame {
    private boolean hasEverStarted = false;
    private ArcalAPIBukkit api;
    private ArcalGameAPI gameApi;

    private MurderMysteryGameMode mode = new ClassicMode();
    private Map<Player, Role> roleMap = new HashMap<>();

    public MurderMystery() {
        this.gameApi = MurderMysteryPlugin.getInstance().getGameApi();
        this.api = this.gameApi.getApi();

        api.chat().log(JSONTextBuilder.text("MurderMystery by Arcal created."));
    }

    public void setMode(MurderMysteryGameMode mode) {
        this.mode.stop();
        this.mode.unload();

        this.mode = mode;
        this.mode.load();
    }

    public MurderMysteryGameMode getMode() {
        return this.mode;
    }

    @Override
    public void start() {
        this.hasEverStarted = true;
        this.mode.start();
    }

    @Override
    public void stop() {
        this.mode.stop();
    }

    @Override
    public void tick(int timer) {
        this.mode.tick(timer);
    }

    @Override
    public void addPlayer(Player p) {
        this.mode.addPlayer(p);
    }

    @Override
    public void removePlayer(Player p) {
        this.mode.removePlayer(p);
    }

    @Override
    public Collection<Player> getPlayers() {
        return this.mode.getPlayers();
    }

    @Override
    public void load() {
        this.mode.load();
        api.chat().log(JSONTextBuilder.text("MurderMystery loaded!").setColor(JSONColor.GREEN));
    }

    @Override
    public void unload() {
        this.mode.unload();
        api.chat().log(JSONTextBuilder.text("MurderMystery unloaded!").setColor(JSONColor.GREEN));
    }

    @Override
    public int getMaxGameTime() {
        return this.mode.getMaxGameTime();
    }

    @Override
    public int getMinPlayerCount() {
        return this.mode.getMinPlayerCount();
    }

    @Override
    public int getMaxCountdownTicks() {
        return this.mode.getMaxCountdownTicks();
    }

    public Role getRoleByPlayer(Player p) {
        return this.roleMap.get(p);
    }

    public void setRoleByPlayer(Player p, Role r) {
        this.roleMap.put(p, r);
    }
}
