package cf.arcal.plugins.game.murder;
import cf.arcal.plugins.api.bukkit.ArcalAPIBukkit;
import cf.arcal.plugins.api.game.ArcalGameAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

// TODO: Test the plugin!!!
public class MurderMysteryPlugin extends JavaPlugin implements Listener {
    private ArcalGameAPI gameApi;
    private ArcalAPIBukkit api;
    private static MurderMysteryPlugin instance;

    @Override
    public void onEnable() {
        instance = this;
        Plugin arcalApi = Bukkit.getPluginManager().getPlugin("ArcalGameAPI");
        if (arcalApi == null) {
            Bukkit.getConsoleSender().sendMessage(
                ChatColor.translateAlternateColorCodes('&', "&cArcalGameAPI not found! Disabling the plugin...")
            );
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }
        this.gameApi = (ArcalGameAPI) arcalApi;
        this.api = ((ArcalGameAPI) arcalApi).getApi();

        gameApi.setGame(new MurderMystery());
    }

    @Override
    public void onDisable() {
        instance = null;
    }

    public static MurderMysteryPlugin getInstance() {
        return instance;
    }

    public ArcalAPIBukkit getApi() {
        return this.api;
    }

    public ArcalGameAPI getGameApi() {
        return this.gameApi;
    }
}
