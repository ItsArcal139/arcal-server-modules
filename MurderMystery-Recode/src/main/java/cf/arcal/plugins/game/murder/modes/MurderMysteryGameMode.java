package cf.arcal.plugins.game.murder.modes;

import cf.arcal.plugins.api.game.ArcalGameAPI;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public abstract class MurderMysteryGameMode {
    private String name;

    public abstract void start();
    public abstract void stop();
    public abstract void tick(int timer);
    public abstract int getMaxGameTime();
    public abstract Collection<Player> getPlayers();
    public abstract void addPlayer(Player p);
    public abstract void removePlayer(Player p);
    public abstract int getMinPlayerCount();

    public abstract void load();
    public abstract void unload();

    protected MurderMysteryGameMode(String name) {
        this.name = name;
    }

    public int getMaxCountdownTicks() {
        return 20 * ArcalGameAPI.TICK_TO_SEC;
    }

    public String getName() {
        return this.name;
    }

    public static MurderMysteryGameMode createByName(String name) {
        Class<? extends MurderMysteryGameMode> c = registry.get(name);
        if(c != null) {
            try {
                return c.getConstructor().newInstance();
            } catch (NoSuchMethodException | InstantiationException | InvocationTargetException | IllegalAccessException ex) {
                return null;
            }
        }
        return null;
    }

    private static Map<String, Class<? extends MurderMysteryGameMode>> registry = new HashMap<>();

    static {
        registry.put("classic", ClassicMode.class);
    }
}
