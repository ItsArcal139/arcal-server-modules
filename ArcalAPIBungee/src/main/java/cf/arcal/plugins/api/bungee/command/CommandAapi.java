package cf.arcal.plugins.api.bungee.command;

import cf.arcal.plugins.api.bungee.*;
import cf.arcal.plugins.api.bungee.util.ServerGroup;
import cf.arcal.plugins.api.util.JSONColor;
import cf.arcal.plugins.api.util.JSONTextBuilder;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.*;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class CommandAapi extends ArcalCommand {
    enum ConnectMode {
        SERVER, GROUP, UNSPECIFIED
    }

    public CommandAapi() {
        super("aapib", "arcalapi.cmduse.aapib", "arcalapi");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            this.showHelp(sender, args);
            return;
        }

        String subcmd = args[0];
        switch (subcmd) {
            case "reload":
                this.doReload(sender);
                break;
            case "connect":
                this.doConnect(sender, args);
                break;
            case "list":
                this.doList(sender, args);
                break;
            default:
                this.doUnknownAction(sender, args);
                this.showHelp(sender, args);
                break;
        }
    }

    /**
     * This command is useful to list the server group and to debug.
     *
     * @param sender the {@code CommandSender}.
     * @param args   the given arguments.
     */
    private void doList(CommandSender sender, String[] args) {
        ArcalAPIBungee api = ArcalAPIBungee.getInstance();
        if (sender.hasPermission(this.getPermission() + ".list")) {
            if (args.length > 2) {
                this.showHelp(sender, args);
                return;
            }

            if (args.length == 1) {
                for (ServerGroup group : api.getServerGroups()) {
                    this._listGroup(sender, group);
                }
                return;
            }

            // Get the server group the user wanted, if we registered it.
            ServerGroup group = api.getServerGroup(args[1]);
            if (group != null) {
                this._listGroup(sender, group);
            } else {
                api.chat().sendToSender(sender, JSONTextBuilder.translatable("Invalid group %s")
                    .addWith(JSONTextBuilder.text(String.format("\"%s\"", args[1])).setColor(JSONColor.GOLD))
                    .setColor(JSONColor.RED)
                );
            }
        }
    }

    private void _listGroup(CommandSender sender, ServerGroup group) {
        ArcalAPIBungee api = ArcalAPIBungee.getInstance();
        if (group != null) {
            api.chat().sendToSender(sender,
                JSONTextBuilder.translatable("The group %s has the following servers:")
                    .addWith(JSONTextBuilder.text(String.format("\"%s\"", group.getName())))
                    .setColor(JSONColor.GOLD)
            );
            for (String name : group.getServers()) {
                api.chat().sendToSender(sender,
                    JSONTextBuilder.translatable(" - %s %s")
                        .addWith(JSONTextBuilder.text("*").setColor(group.isServerOnline(name) ? JSONColor.GREEN : JSONColor.RED))
                        .addWith(JSONTextBuilder.text(name).setColor(JSONColor.WHITE))
                        .setColor(JSONColor.GRAY)
                );
            }
        }
    }

    private void doConnect(CommandSender sender, String[] args) {
        ArcalAPIBungee api = ArcalAPIBungee.getInstance();
        if (sender.hasPermission(this.getPermission() + ".connect")) {
            if (args.length < 3 || args.length > 4) {
                this.showHelp(sender, args);
                return;
            }
            ConnectMode mode = ConnectMode.UNSPECIFIED;
            switch (args[1].toLowerCase()) {
                case "server":
                    mode = ConnectMode.SERVER;
                    break;
                case "section":
                case "group":
                    mode = ConnectMode.GROUP;
            }
            if (mode == ConnectMode.UNSPECIFIED) {
                api.chat().sendToSender(sender, JSONTextBuilder.text("Please specify a valid connect mode.").setColor(JSONColor.RED));
                api.chat().sendToSender(sender, JSONTextBuilder.text("Valid options: server, group").setColor(JSONColor.GOLD));
                return;
            }

            ProxiedPlayer p = null;
            if (sender instanceof ProxiedPlayer) {
                p = (ProxiedPlayer) sender;
            }
            if (args.length == 4) {
                p = BungeeCord.getInstance().getPlayer(args[3]);
                if (p == null) {
                    api.chat().sendToSender(sender,
                        JSONTextBuilder.translatable("Couldn't find player %s!")
                            .addWith(JSONTextBuilder.text(String.format("\"%s\"", args[3])).setColor(JSONColor.GOLD))
                            .setColor(JSONColor.RED)
                    );
                    return;
                }
            }
            if (p == null) {
                api.chat().sendToSender(sender, JSONTextBuilder.text("You have to specify a player.").setColor(JSONColor.RED));
                return;
            }
            try {
                switch (mode) {
                    case SERVER:
                        api.bungee().sendPlayerToServer(p, BungeeCord.getInstance().getServerInfo(args[2]));
                        break;
                    case GROUP:
                        api.bungee().sendPlayerToGroup(p, api.getServerGroup(args[2]), false);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                api.chat().sendToSender(sender, JSONTextBuilder.text(ex.getMessage()).setColor(JSONColor.RED));
            }
        } else {
            api.chat().sendToSender(sender, JSONTextBuilder.text("You don't have permission to do this.").setColor(JSONColor.RED));
        }
    }

    private void doReload(CommandSender sender) {
        ArcalAPIBungee api = ArcalAPIBungee.getInstance();
        if (sender.hasPermission(this.getPermission() + ".reload")) {
            api.chat().sendToSender(sender, JSONTextBuilder.text("Reloading configs...").setColor(JSONColor.GREEN));
            api.reload();
        } else {
            api.chat().sendToSender(sender, JSONTextBuilder.text("You don't have permission to do this.").setColor(JSONColor.RED));
        }
    }

    private void doUnknownAction(CommandSender sender, String[] args) {
        ArcalAPIBungee api = ArcalAPIBungee.getInstance();
        api.chat().sendToSender(sender,
            JSONTextBuilder.translatable("Unknown subcommand %s.")
                .addWith(JSONTextBuilder.text(String.format("\"%s\"", args[0])).setColor(JSONColor.WHITE))
                .setColor(JSONColor.RED)
        );
    }

    private void showHelp(CommandSender sender, String[] args) {
        ArcalAPIBungee api = ArcalAPIBungee.getInstance();
        api.chat().sendToSender(sender, JSONTextBuilder.text("Usage:"));

        String format = "&6  %s &f- %s";
        if (sender.hasPermission("arcalapi.cmdsee.aapib.reload")) {
            api.chat().sendToSender(sender,
                JSONTextBuilder.translatable(format)
                    .addWith(JSONTextBuilder.text("/" + this.getName() + " reload").setColor(JSONColor.GOLD))
                    .addWith(JSONTextBuilder.text("Reloads the configuration."))
                    .alternativeColorCode('&').setColor(JSONColor.WHITE)
            );
        }
        if (sender.hasPermission("arcalapi.cmdsee.aapib.connect")) {
            api.chat().sendToSender(sender,
                JSONTextBuilder.translatable(format)
                    .addWith(JSONTextBuilder.text("/" + this.getName() + " connect <server|group> <name> [player]").setColor(JSONColor.GOLD))
                    .addWith(JSONTextBuilder.text("Brings player to the specific server or group."))
                    .alternativeColorCode('&').setColor(JSONColor.WHITE)
            );
        }
    }
}
