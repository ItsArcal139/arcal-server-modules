package cf.arcal.plugins.api.bungee.placeholder;

import net.md_5.bungee.*;
import net.md_5.bungee.api.connection.*;

/**
 * @author Arcal
 */
public class AapiExpansion extends Expansion {
    @Override
    public String getName() {
        return "AapiExpansion";
    }

    @Override
    public String getIdentifier() {
        return "aapi";
    }

    @Override
    public String getAuthor() {
        return "Arcal";
    }

    @Override
    public String onPlaceholderRequest(ProxiedPlayer p, String identifier) {
        if (identifier.equals("players")) {
            return String.valueOf(BungeeCord.getInstance().getOnlineCount());
        }
        if (identifier.equals("player_name")) {
            return p.getName();
        }
        return null;
    }

}
