package cf.arcal.plugins.api.bungee;

import java.io.IOException;
import java.util.*;

import cf.arcal.plugins.api.ArcalAPIPlatform;
import cf.arcal.plugins.api.bungee.command.ArcalCommand;
import cf.arcal.plugins.api.bungee.config.ArcalAPIBungeeConfig;
import cf.arcal.plugins.api.bungee.config.BungeeConfigProvider;
import cf.arcal.plugins.api.bungee.placeholder.PlaceholderAPI;
import cf.arcal.plugins.api.bungee.util.BungeeChatUtility;
import cf.arcal.plugins.api.bungee.util.BungeeJSONTextBuilder;
import cf.arcal.plugins.api.bungee.util.BungeeUtility;
import cf.arcal.plugins.api.bungee.util.ServerGroup;
import cf.arcal.plugins.api.bungee.util.TextUtility;
import cf.arcal.plugins.api.config.ConfigProvider;
import cf.arcal.plugins.api.util.JSONColor;
import cf.arcal.plugins.api.util.JSONComponent;
import cf.arcal.plugins.api.util.JSONTextBuilder;
import cf.arcal.plugins.api.util.JSONTextComponent;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.event.ProxyReloadEvent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.connection.*;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.protocol.ProtocolConstants;

import java.io.File;

public class ArcalAPIBungee extends Plugin implements Listener, ArcalAPIPlatform {
    private BungeeChatUtility chatUtil;
    private static ArcalAPIBungee instance = null;
    private ArcalAPIBungeeConfig config = null;
    private BungeeUtility bungee = null;
    private boolean isLobbyEnabled = false;
    private ConfigProvider provider;
    private String defaultLobbyGroup = null;

    private List<ServerGroup> serverGroups = new ArrayList<>();

    public List<ServerGroup> getServerGroups() {
        return this.serverGroups;
    }

    public void setLobbyEnabled(boolean flag) {
        this.isLobbyEnabled = flag;
    }

    public void setDefaultLobbyGroup(String name) {
        this.defaultLobbyGroup = name;
    }

    public String getDefaultLobbyGroup() {
        return this.defaultLobbyGroup;
    }

    public boolean isLobbyEnabled() {
        return this.isLobbyEnabled;
    }

    public ConfigProvider getConfigProvider() {
        if (provider == null) {
            provider = new BungeeConfigProvider(this, null);
        }
        return provider;
    }

    @Override
    public void onEnable() {
        this.getProxy().getPluginManager().registerListener(this, this);
        ArcalCommand.registerCommands(this, this.getProxy().getPluginManager());
        ArcalAPIBungee.instance = this;

        this.chatUtil = new BungeeChatUtility();
        this.bungee = new BungeeUtility();
        this.config = ArcalAPIBungeeConfig.getInstance();


    }

    public BungeeUtility bungee() {
        return this.bungee;
    }

    public static ArcalAPIBungee getInstance() {
        return ArcalAPIBungee.instance;
    }

    @SuppressWarnings("unchecked")
    public ArcalAPIBungeeConfig config() {
        return this.config;
    }

    public BungeeChatUtility chat() {
        return this.chatUtil;
    }

    @EventHandler
    public void onPreLogin(PreLoginEvent event) {
        PendingConnection conn = event.getConnection();
        this.chat().log(
            JSONTextBuilder.translatable("A pending connection attempted to login with protocol version %s.")
                .addWith(JSONTextBuilder.text(String.valueOf(conn.getVersion())).setColor(JSONColor.GOLD))
                .setColor(JSONColor.WHITE)
        );
    }

    @EventHandler
    public void onPostLogin(PostLoginEvent event) {
        ProxiedPlayer p = event.getPlayer();
        if (this.config().isPlayerProxyJoinMsgEnabled()) {
            this.chat().broadcast(JSONTextBuilder.text(PlaceholderAPI.parse(p, this.config.getPlayerProxyJoinMsg())));
        }

        this.chat().log(
            JSONTextBuilder.translatable("Player %s joined with protocol version %s.")
                .addWith(JSONTextBuilder.text(p.getName()).setColor(JSONColor.GOLD))
                .addWith(JSONTextBuilder.text(String.valueOf(p.getPendingConnection().getVersion())).setColor(JSONColor.GOLD))
        );
    }

    @EventHandler
    public void onServerSwitch(ServerSwitchEvent event) {
        ProxiedPlayer p = event.getPlayer();
        Server s = p.getServer();
        this.chat().log(
            JSONTextBuilder.translatable("Player %s has switched to the server %s.")
                .addWith(JSONTextBuilder.text(p.getName()).setColor(JSONColor.GOLD))
                .addWith(JSONTextBuilder.text(s.getInfo().getName())).setColor(JSONColor.GOLD)
        );
    }

    @EventHandler
    public void onServerConnect(ServerConnectEvent event) {
        ProxiedPlayer p = event.getPlayer();
        ServerInfo s = event.getTarget();
        this.chat().sendToPlayer(p, JSONTextBuilder.translatable("Sending you to %s!")
            .addWith(JSONTextBuilder.text(s.getName()))
            .setColor(JSONColor.GREEN)
        );
    }

    public JSONTextComponent getPlayerComponent(ProxiedPlayer p) {
        return (JSONTextComponent) new JSONTextComponent(p.getName())
            .setHoverEvent(JSONComponent.HoverEvent.showEntity(p.getName(), "Player", p.getUniqueId()))
            .setClickEvent(JSONComponent.ClickEvent.suggestCommand("/msg " + p.getName() + " "));
    }

    private String getVersionFromProtocol(int ver) {
        switch (ver) {
            case ProtocolConstants.MINECRAFT_1_8:
                return "1.8";
            case ProtocolConstants.MINECRAFT_1_9:
                return "1.9";
            case ProtocolConstants.MINECRAFT_1_9_1:
                return "1.9.1";
            case ProtocolConstants.MINECRAFT_1_9_2:
                return "1.9.2";
            case ProtocolConstants.MINECRAFT_1_9_4:
                return "1.9.4";
            case ProtocolConstants.MINECRAFT_1_10:
                return "1.10";
            case ProtocolConstants.MINECRAFT_1_11:
                return "1.11";
            case ProtocolConstants.MINECRAFT_1_11_1:
                return "1.11.1";
            case ProtocolConstants.MINECRAFT_1_12:
                return "1.12";
            case ProtocolConstants.MINECRAFT_1_12_1:
                return "1.12.1";
            case ProtocolConstants.MINECRAFT_1_12_2:
                return "1.12.2";

            // Since we have an old BungeeCord lib and I am lazy,
            // we use the protocol number from now here.
            case 393: // 1.13
                return "1.13";
            case 401: // 1.13.1
                return "1.13.1";
            case 404: // 1.13.2
                return "1.13.2";

            case 455:
                return "19w03c";
            case 456:
                return "19w04a";

            default:
                return "Unknown";
        }
    }

    @EventHandler
    public void onProxyPing(ProxyPingEvent event) {
        PendingConnection conn = event.getConnection();
        // this.chat().log("A player with protocol version &6"+conn.getVersion()+" &fsent a ping request.");
        ServerPing ping = event.getResponse();

        List<String> motds = this.config.getMOTD();
        String motd = motds.get(0) + "\n" + motds.get(1);
        motd = ChatColor.translateAlternateColorCodes('&', motd);
        BaseComponent[] cs = TextComponent.fromLegacyText(motd);
        BaseComponent c = cs[0];
        for (int i = 1; i < cs.length; i++) {
            c.addExtra(cs[i]);
        }

        ping.setDescriptionComponent(c);

        if (conn.getVersion() >= 47 && this.config.isFakeProtocolEnabled()) {
            StringBuilder result = new StringBuilder(ChatColor.translateAlternateColorCodes('&', this.config.getFloatLeftText()));

            ServerPing.Protocol protocol = ping.getVersion();
            int p = protocol.getProtocol() + 1;
            String pName = ChatColor.translateAlternateColorCodes('&', "&r" + this.config.getFakeProtocolName());
            pName = PlaceholderAPI.parse(null, pName);

            boolean isServerTooOld = ProtocolConstants.SUPPORTED_VERSION_IDS.get(ProtocolConstants.SUPPORTED_VERSION_IDS.size() - 1) < conn.getVersion();

            ServerPing.Players spl = ping.getPlayers();
            spl.setSample(new ServerPing.PlayerInfo[]{
                new ServerPing.PlayerInfo(ChatColor.translateAlternateColorCodes('&', "&fYour protocol version: &"
                    + (isServerTooOld ? "c" : "a")
                    + conn.getVersion()), UUID.randomUUID()),
                new ServerPing.PlayerInfo(ChatColor.translateAlternateColorCodes('&', "&fYour version: &"
                    + (isServerTooOld ? "c" : "a")
                    + getVersionFromProtocol(conn.getVersion())), UUID.randomUUID())
            });

            // To hide the "and other ..." text at the bottom, we set
            // the player count to zero.
            spl.setOnline(0);

            if (isServerTooOld) {
                p -= 2; // To make it really too old. (?
            }

            int spaceCount = TextUtility.getServerListSpacePad(ChatColor.stripColor(pName), this.config.getServerListSize());
            for (int i = 0; i < spaceCount; i++) {
                result.append(" ");
            }
            result.append(pName);

            protocol.setName(result.toString());
            protocol.setProtocol(p);
            ping.setVersion(protocol);
        }

        event.setResponse(ping);
    }

    @EventHandler
    public void onPlayerDisconnect(PlayerDisconnectEvent event) {
        ProxiedPlayer p = event.getPlayer();
        if (this.config().isPlayerProxyLeftMsgEnabled()) {
            this.chat().broadcast(JSONTextBuilder.text(PlaceholderAPI.parse(p, this.config.getPlayerProxyLeftMsg())));
        }
    }

    @EventHandler
    public void onServerKick(ServerKickEvent event) {
        ProxiedPlayer p = event.getPlayer();
        ServerInfo server = event.getKickedFrom();

        // Debug message of being kicked.
        this.chat().log(
            JSONTextBuilder.translatable("Player %s was kicked from the server %s.")
                .addWith(JSONTextBuilder.text(p.getName()).setColor(JSONColor.GOLD))
                .addWith(JSONTextBuilder.text(server.getName()).setColor(JSONColor.GOLD))
                .setColor(JSONColor.WHITE)
        );

        // this.chat().log("&7Reason: &f" + TextComponent.toLegacyText(event.getKickReasonComponent()).replace("\\\u00a7", "&").replace("&", "&&f"));
        // this.chat().log("&7Reason (colored): &f" + TextComponent.toLegacyText(event.getKickReasonComponent()));

        this.chat().log(
            JSONTextBuilder.text("Reason: ")
                .setColor(JSONColor.GRAY)
                .addExtra(BungeeJSONTextBuilder.fromBaseComponent(event.getKickReasonComponent()).getColorlessClone())
        );
        this.chat().log(
            JSONTextBuilder.text("Reason (colored): ")
                .setColor(JSONColor.GRAY)
                .addExtra(BungeeJSONTextBuilder.fromBaseComponent(event.getKickReasonComponent()))
        );

        ServerGroup group = null;
        for (ServerGroup g : this.serverGroups) {
            if (g.containsServer(server.getName())) {
                group = g;
                break;
            }
        }

        // Choose a destination server.
        ServerGroup g = this.getServerGroup("lobby-" + this.defaultLobbyGroup);
        ServerInfo s = g.chooseServer();

        if (s == null) {
            this.chat().log(
                JSONTextBuilder.translatable("Group \"lobby-%s\" returns null!")
                    .addWith(JSONTextBuilder.text(this.defaultLobbyGroup))
                    .setColor(JSONColor.RED)
            );
        }

        if (group == null) {
            this.chat().log(
                JSONTextBuilder.translatable("The server %s is not registered to any group!")
                    .addWith(JSONTextBuilder.text(server.getName()))
                    .setColor(JSONColor.RED)
            );
        }

        if (!isLobbyEnabled) {
            this.chat().log(JSONTextBuilder.text("The lobby is not set!").setColor(JSONColor.RED));
        }

        if (s == null || (group != null && group.getName().startsWith("lobby-")) || !isLobbyEnabled) {
            // Modify the kick message, then bye.
            BaseComponent[] c = event.getKickReasonComponent();
            ComponentBuilder builder = new ComponentBuilder("");

            int lineCounter = 0;

            List<String> lines = this.config().getPlayerKickedMsg();

            if (TextComponent.toLegacyText(c).startsWith("Banned: ")) {
                lines = this.config().getPlayerBannedMsg();
                c = TextComponent.fromLegacyText(TextComponent.toLegacyText(c).substring("Banned: ".length()));
            }

            for (String line : lines) {
                String processed = line;
                processed = "&f" + processed.replace("{reason}", TextComponent.toPlainText(c)) + (lineCounter + 1 == this.config().getPlayerKickedMsg().size() ? "" : "\n");
                BaseComponent[] l = TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', processed));
                builder.append(l);
                lineCounter++;
            }

            event.setKickReasonComponent(builder.create());
            return;
        }

        this.chat().log(
            JSONTextBuilder.translatable("Sending %s to lobby %s...")
                .addWith(JSONTextBuilder.text(p.getName()).setColor(JSONColor.GOLD))
                .addWith(JSONTextBuilder.text(s.getName()).setColor(JSONColor.GOLD))
                .setColor(JSONColor.WHITE)
        );
        event.setCancelled(true);
        this.bungee().sendPlayerToServer(p, s);
    }

    @EventHandler
    public void onProxyReload(ProxyReloadEvent event) {
        this.reload();
    }

    public void addServerGroup(ServerGroup g) {
        this.serverGroups.add(g);
    }

    public ServerGroup getServerGroup(String name) {
        ServerGroup result = null;
        for (ServerGroup s : this.serverGroups) {
            if (s.getName().equals(name)) result = s;
        }
        return result;
    }

    public void reload() {
        for (ServerGroup g : this.getServerGroups()) {
            g.unregisterAllServer();
        }
        this.serverGroups.clear();
        this.chat().log(JSONTextBuilder.text("Reloading configs...").setColor(JSONColor.GREEN));
        this.config.loadConfig();
    }

    @Override
    public void onDisable() {
        ArcalAPIBungee.instance = null;
    }

    public ConfigProvider createConfigProviderByLoadingFile(File file) throws IOException {
        ConfigProvider provider = this.getConfigProvider();
        provider.load(file);
        return provider;
    }

    @Override
    public int getOnlinePlayerCount() {
        return BungeeCord.getInstance().getOnlineCount();
    }

    @Override
    public int getMaxPlayers() {
        return BungeeCord.getInstance().getConfig().getPlayerLimit();
    }
}
