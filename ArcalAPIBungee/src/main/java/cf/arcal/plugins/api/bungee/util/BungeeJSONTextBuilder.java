package cf.arcal.plugins.api.bungee.util;

import cf.arcal.plugins.api.util.*;
import net.md_5.bungee.api.*;
import net.md_5.bungee.api.chat.*;

public class BungeeJSONTextBuilder {
    public static BaseComponent[] build(JSONComponent text) {
        BaseComponent comp = null;
        if (text instanceof JSONTextComponent) {
            comp = buildText((JSONTextComponent) text);
        } else if (text instanceof JSONSelectorComponent) {
            comp = buildSelector((JSONSelectorComponent) text);
        } else if (text instanceof JSONKeybindComponent) {
            comp = buildKeybind((JSONKeybindComponent) text);
        } else if (text instanceof JSONTranslatableComponent) {
            comp = buildTranslatable((JSONTranslatableComponent) text);
        } else if (text instanceof JSONScoreComponent) {
            comp = buildScore((JSONScoreComponent) text);
        } else {
            comp = buildText((JSONTextComponent) new JSONTextComponent("<invalid>").setColor(JSONColor.RED).setExtras(text.getExtras()));
        }

        comp.setItalic(text.isItalic());
        comp.setBold(text.isBold());
        comp.setObfuscated(text.isObfuscated());
        comp.setInsertion(text.getInsertion());
        comp.setUnderlined(text.isUnderlined());
        comp.setStrikethrough(text.isStrikethrough());

        if (text.getColor() != null) {
            comp.setColor(ChatColor.valueOf(text.getColor().name()));
        }

        if (text.getClickEvent() != null) {
            JSONComponent.ClickEvent event = text.getClickEvent();
            ClickEvent ev = new ClickEvent(ClickEvent.Action.valueOf(event.getAction().name()), event.getValue());
            comp.setClickEvent(ev);
        }

        if (text.getHoverEvent() != null) {
            JSONComponent.HoverEvent event = text.getHoverEvent();
            HoverEvent ev = new HoverEvent(HoverEvent.Action.valueOf(event.getAction().name()), build(event.getValue()));
            comp.setHoverEvent(ev);
        }

        for (JSONComponent child : text.getExtras()) {
            comp.addExtra(build(child)[0]);
        }

        BaseComponent[] result = new BaseComponent[1];
        result[0] = comp;
        return result;
    }

    private static TextComponent buildText(JSONTextComponent text) {
        TextComponent comp = new TextComponent();
        comp.setText(text.getText());
        return comp;
    }

    private static SelectorComponent buildSelector(JSONSelectorComponent text) {
        return new SelectorComponent(text.getSelector());
    }

    private static KeybindComponent buildKeybind(JSONKeybindComponent text) {
        return new KeybindComponent(text.getKeybind());
    }

    private static TranslatableComponent buildTranslatable(JSONTranslatableComponent text) {
        TranslatableComponent comp = new TranslatableComponent(text.getTranslate());
        for (JSONComponent item : text.getWiths()) {
            comp.addWith(build(item)[0]);
        }
        return comp;
    }

    private static ScoreComponent buildScore(JSONScoreComponent text) {
        JSONScoreComponent.Score score = text.getScore();
        return new ScoreComponent(score.getName(), score.getObjective());
    }

    public static JSONComponent fromBaseComponent(BaseComponent[] comp) {
        if (comp.length == 0) {
            return JSONTextBuilder.text("");
        }

        JSONComponent result = fromBaseComponent(comp[0]);
        if (comp.length == 1) {
            return result;
        }

        for (int i = 1; i < comp.length; i++) {
            result.addExtra(fromBaseComponent(comp[i]));
        }
        return result;
    }

    public static JSONComponent fromBaseComponent(BaseComponent comp) {
        JSONComponent result;
        if (comp instanceof TextComponent) {
            result = JSONTextBuilder.text(((TextComponent) comp).getText());
        } else if (comp instanceof TranslatableComponent) {
            result = JSONTextBuilder.translatable(((TranslatableComponent) comp).getTranslate());
        } else {
            result = JSONTextBuilder.text("");
        }

        JSONComponent.ClickAction clickAction = JSONComponent.ClickAction.valueOf(comp.getClickEvent().getAction().name());
        result.setClickEvent(new JSONComponent.ClickEvent(clickAction, comp.getClickEvent().getValue()));

        JSONComponent.HoverAction hoverAction = JSONComponent.HoverAction.valueOf(comp.getHoverEvent().getAction().name());
        result.setHoverEvent(new JSONComponent.HoverEvent(hoverAction, fromBaseComponent(comp.getHoverEvent().getValue())));

        result.setInsertion(comp.getInsertion());
        result.setObfuscated(comp.isObfuscated());
        result.setItalic(comp.isItalic());
        result.setBold(comp.isBold());
        result.setUnderlined(comp.isUnderlined());
        result.setStrikethrough(comp.isStrikethrough());
        result.setColor(JSONColor.valueOf(comp.getColor().name()));

        for (BaseComponent extra : comp.getExtra()) {
            result.addExtra(fromBaseComponent(extra));
        }
        return result;
    }
}
