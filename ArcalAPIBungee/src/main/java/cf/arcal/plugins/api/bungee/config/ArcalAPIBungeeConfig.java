package cf.arcal.plugins.api.bungee.config;

import cf.arcal.plugins.api.*;
import cf.arcal.plugins.api.bungee.*;
import cf.arcal.plugins.api.bungee.util.*;
import cf.arcal.plugins.api.config.*;

import java.io.*;
import java.util.*;

import cf.arcal.plugins.api.util.JSONColor;
import cf.arcal.plugins.api.util.JSONTextBuilder;
import net.md_5.bungee.config.*;

public class ArcalAPIBungeeConfig extends BaseAPIConfig<Configuration> {
    private static ArcalAPIBungeeConfig instance = null;

    public static ArcalAPIBungeeConfig getInstance() {
        if (instance == null) {
            instance = new ArcalAPIBungeeConfig();
        }
        return instance;
    }

    public static void resetInstance() {
        instance = null;
    }

    @Override
    public ArcalAPIPlatform getAPI() {
        return ArcalAPIBungee.getInstance();
    }

    private ConfigurationProvider provider;
    private File configFile;
    private String motdLine1 = null;
    private String motdLine2 = null;
    private boolean enableFakeProtocol = false;
    private int serverListSize = -1;
    private String protocolName = "";
    private List<String> floatLeftTexts = null;

    private boolean enablePlayerProxyJoinMsg = true;
    private boolean enablePlayerProxyLeftMsg = true;

    private String pluginPrefix = null;
    private String playerProxyJoinMsg = null;
    private String playerProxyLeftMsg = null;

    private List<String> playerSendToServerMsg = null;
    private List<String> playerKickedSendMsg = null;
    private List<String> playerUnableToSendMsg = null;

    private List<String> playerKickedMsg = null;
    private List<String> playerBannedMsg = null;

    private ServerGroup defaultLobbyGroup = null;

    private ArcalAPIBungeeConfig() {
        this.loadConfig();
    }

    private List<String> getStringListEnhanced(String key) {
        return this.getStringListEnhanced(key, null);
    }

    private List<String> getStringListEnhanced(String key, String def) {
        List<String> result = config.getStringList(key);
        if (result.size() == 0) {    // Maybe a string?
            result = new ArrayList<>();
            result.add(config.getString(key, def));
        }
        return result;
    }

    @Override
    public final boolean loadConfig() {
        ArcalAPIBungee api = ArcalAPIBungee.getInstance();
        boolean prevResult = super.loadConfig();

        if (prevResult && this.config != null) {
            this.pluginPrefix = config.getString("plugin-prefix", " &7▏ &6Arcal&fNetwork &7▏ &f");

            this.enablePlayerProxyJoinMsg = config.getBoolean("enable-player-proxy-join-message", true);
            this.enablePlayerProxyLeftMsg = config.getBoolean("enable-player-proxy-left-message", true);

            this.playerProxyJoinMsg = config.getString("player-proxy-join-message", "&c%aapi_player_name% &fhas joined the network!");
            this.playerProxyLeftMsg = config.getString("player-proxy-left-message", "&c%aapi_player_name% &fhas left the network!");

            this.playerSendToServerMsg = this.getStringListEnhanced("player-send-to-server-message");
            this.playerKickedSendMsg = this.getStringListEnhanced("player-kicked-send-message");
            this.playerUnableToSendMsg = this.getStringListEnhanced("player-unable-to-send-message");

            this.playerKickedMsg = this.getStringListEnhanced("player-kicked-message");
            this.playerBannedMsg = this.getStringListEnhanced("player-banned-message");

            List<String> motdLines = config.getStringList("motd-lines");
            this.motdLine1 = motdLines.isEmpty() ? config.getString("motd-lines", "A Minecraft Server") : motdLines.get(0);
            this.motdLine2 = motdLines.isEmpty() ? "" : (motdLines.size() >= 2 ? motdLines.get(1) : "");

            this.enableFakeProtocol = config.getBoolean("enable-fake-protocol", false);
            this.protocolName = config.getString("fake-protocol-name", "&fA &6Good &fServer!");

            this.serverListSize = config.getInt("server-list-size", 288);

            List<String> floatLeftTexts = config.getStringList("float-left-text");
            if (floatLeftTexts.isEmpty()) {
                String floatLeftText = config.getString("float-left-text");
                floatLeftTexts = new ArrayList<>();
                if (floatLeftText != null) floatLeftTexts.add(floatLeftText);
            }
            this.floatLeftTexts = floatLeftTexts;

            // Server groups set.
            Configuration serverGroupsSec = config.getSection("server-groups");
            if (serverGroupsSec != null) {
                for (String key : serverGroupsSec.getKeys()) {
                    Configuration g = serverGroupsSec.getSection(key);
                    if (g != null) {
                        String serverRegex = g.getString("servers");
                        String algorithmStr = g.getString("algorithm");
                        ServerGroup.Algorithm algorithm = ServerGroup.Algorithm.valueOf(algorithmStr.toUpperCase());
                        ServerGroup sg = new ServerGroup(key, algorithm);
                        sg.registerServerByRegExp(serverRegex);
                        api.getServerGroups().add(sg);
                    }
                }
            }

            // Lobby group set.
            Configuration lobbyGroupsSec = config.getSection("lobby-group");
            if (lobbyGroupsSec != null) {
                String defaultGroup = lobbyGroupsSec.getString("default");
                Configuration groups = lobbyGroupsSec.getSection("groups");
                if (groups == null || groups.getKeys().isEmpty()) {
                    api.chat().log(JSONTextBuilder.text("Lobby groups are not set! The /lobby command will not be usable.").setColor(JSONColor.RED));
                } else {
                    api.setLobbyEnabled(true);
                    if (defaultGroup == null) {
                        try {
                            defaultGroup = groups.getKeys().iterator().next();
                            api.chat().log(JSONTextBuilder.text("A default lobby group is not set!").setColor(JSONColor.RED));
                            api.chat().log(
                                JSONTextBuilder.translatable("The default lobby group will be set to %s since it is the first one.")
                                    .addWith(JSONTextBuilder.text(defaultGroup).setColor(JSONColor.GOLD))
                                    .setColor(JSONColor.WHITE)
                            );
                        } catch (NoSuchElementException ex) {
                            api.chat().log(JSONTextBuilder.text("NoSuchElementException thrown!???").setColor(JSONColor.RED));
                            ex.printStackTrace();
                        }
                    }

                    api.setDefaultLobbyGroup(defaultGroup);

                    boolean validDef = false;
                    for (String key : groups.getKeys()) {
                        Configuration g = groups.getSection(key);
                        if (g != null) {
                            String serverRegex = g.getString("servers");
                            String algorithmStr = g.getString("algorithm");
                            ServerGroup.Algorithm algorithm = ServerGroup.Algorithm.valueOf(algorithmStr.toUpperCase());
                            ServerGroup sg = new ServerGroup("lobby-" + key, algorithm);
                            sg.registerServerByRegExp(serverRegex);
                            api.getServerGroups().add(sg);

                            validDef = validDef || key.equals(defaultGroup);
                        }
                    }
                    if (!validDef) {
                        defaultGroup = groups.getKeys().iterator().next();
                        api.chat().log(JSONTextBuilder.text("Invalid default lobby group was set!").setColor(JSONColor.RED));
                        api.chat().log(
                            JSONTextBuilder.translatable("The default lobby group will be set to %s since it is the first one.")
                                .addWith(JSONTextBuilder.text(defaultGroup).setColor(JSONColor.GOLD))
                                .setColor(JSONColor.WHITE)
                        );
                        api.setDefaultLobbyGroup(defaultGroup);
                    }
                }
            }
        } else {
            return false;
        }

        api.chat().log(JSONTextBuilder.text("Configuration loaded!").setColor(JSONColor.GREEN));
        return true;
    }

    private List<String> getStringList(String key) {
        List<String> result = this.config.getStringList(key);
        if (result.isEmpty()) {
            result = new ArrayList<>();
            result.add(this.config.getString(key));
        }
        return result;
    }

    public String getFakeProtocolName() {
        return this.protocolName;
    }

    public ServerGroup getDefaultLobbyGroup() {
        return this.defaultLobbyGroup;
    }

    public String getFloatLeftText() {
        if (this.getFloatLeftTexts() != null && this.getFloatLeftTexts().size() > 0) {
            return this.getFloatLeftTexts().get((int) Math.floor(this.getFloatLeftTexts().size() * Math.random()));
        }
        return "";
    }

    public List<String> getFloatLeftTexts() {
        return this.floatLeftTexts;
    }

    public boolean isFakeProtocolEnabled() {
        return this.enableFakeProtocol;
    }

    public int getServerListSize() {
        return this.serverListSize;
    }

    public String getPluginPrefix() {
        return this.pluginPrefix;
    }

    public String getPlayerProxyJoinMsg() {
        return this.playerProxyJoinMsg;
    }

    public String getPlayerProxyLeftMsg() {
        return this.playerProxyLeftMsg;
    }

    public List<String> getPlayerSendToServerMsg() {
        return this.playerSendToServerMsg;
    }

    public List<String> getPlayerKickedSendMsg() {
        return this.playerKickedSendMsg;
    }

    public List<String> getPlayerUnableToSendMsg() {
        return this.playerUnableToSendMsg;
    }

    public List<String> getPlayerKickedMsg() {
        return this.playerKickedMsg;
    }

    public List<String> getPlayerBannedMsg() {
        return this.playerBannedMsg;
    }

    @Override
    public void saveConfig() {
        // ;
    }

    public List<String> getMOTD() {
        List<String> result = new ArrayList<>();
        result.add(motdLine1);
        result.add(motdLine2);
        return result;
    }

    public boolean isPlayerProxyJoinMsgEnabled() {
        return this.enablePlayerProxyJoinMsg;
    }

    public boolean isPlayerProxyLeftMsgEnabled() {
        return this.enablePlayerProxyLeftMsg;
    }
}
