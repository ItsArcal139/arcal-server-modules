package cf.arcal.plugins.api.bungee.placeholder;

import net.md_5.bungee.api.connection.*;

/**
 * Inspired by the Bukkit plugin PlaceholderAPI by clip.
 *
 * @author Arcal
 */
public abstract class Expansion {
    public abstract String getName();

    public abstract String getIdentifier();

    public abstract String getAuthor();

    public abstract String onPlaceholderRequest(ProxiedPlayer p, String identifier);
}
