package cf.arcal.plugins.api.bungee.util;

import cf.arcal.plugins.api.util.JSONTextBuilder;
import net.md_5.bungee.api.connection.*;
import cf.arcal.plugins.api.bungee.*;
import cf.arcal.plugins.api.bungee.placeholder.*;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.config.*;

import java.util.*;

public class BungeeUtility {
    private ArcalAPIBungee api = null;

    public BungeeUtility() {
        this.api = ArcalAPIBungee.getInstance();
    }

    public void sendPlayerToServer(ProxiedPlayer p, ServerInfo s) {
        this.sendPlayerToServer(p, s, true);
    }

    public void sendPlayerToServer(ProxiedPlayer p, ServerInfo s, boolean needPing) {
        if (s == null) {
            throw new RuntimeException("Invalid server!");
        }

        if (!needPing) {
            this._sendToServer(p, s);
            return;
        }
        s.ping((ServerPing ping, Throwable t) -> {
            if (t == null) {
                this._sendToServer(p, s);
            } else {
                List<String> msg = api.config().getPlayerUnableToSendMsg();
                for (String l : msg) {
                    String line = l.replace("{server}", s.getName());
                    line = PlaceholderAPI.parse(p, line);
                    api.chat().sendToPlayer(p, JSONTextBuilder.text(PlaceholderAPI.parse(p, line)));
                }
            }
        });
    }

    private void _sendToServer(ProxiedPlayer p, ServerInfo s) {
        List<String> msg = api.config().getPlayerSendToServerMsg();
        for (String l : msg) {
            String line = l.replace("{server}", s.getName());
            line = PlaceholderAPI.parse(p, line);
            api.chat().sendToPlayer(p, JSONTextBuilder.text(PlaceholderAPI.parse(p, line)));
        }
        p.connect(s);
    }

    public void sendPlayerToGroup(ProxiedPlayer p, ServerGroup g) {
        this.sendPlayerToGroup(p, g, true);
    }

    public void sendPlayerToGroup(ProxiedPlayer p, ServerGroup g, boolean needPing) {
        if (g == null) {
            throw new RuntimeException("Invalid server group!");
        }

        this.sendPlayerToServer(p, g.chooseServer(), needPing);
    }
}
