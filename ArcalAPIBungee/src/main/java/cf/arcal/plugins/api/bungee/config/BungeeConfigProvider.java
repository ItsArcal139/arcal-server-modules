package cf.arcal.plugins.api.bungee.config;

import cf.arcal.plugins.api.bungee.*;
import cf.arcal.plugins.api.config.*;

import java.io.*;

import net.md_5.bungee.config.*;

/**
 * @author Arcal
 */
public class BungeeConfigProvider extends ConfigProvider<Configuration> {
    public BungeeConfigProvider(ArcalAPIBungee api, Configuration config) {
        super(api, config);
    }

    @Override
    public Configuration load(File file) throws IOException {
        return ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
    }

    @Override
    public String getString(String name) {
        return this.getConfig().getString(name);
    }

    @Override
    public void setString(String name, String value) {
        this.getConfig().set(name, value);
    }
}
