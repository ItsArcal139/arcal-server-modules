package cf.arcal.plugins.api.bungee.command;

import net.md_5.bungee.api.plugin.*;

public abstract class ArcalCommand extends Command {
    protected ArcalCommand(String name) {
        super(name);
    }

    protected ArcalCommand(String name, String permission, String... aliases) {
        super(name, permission, aliases);
    }

    public static void registerCommands(Plugin pl, PluginManager pm) {
        pm.registerCommand(pl, new CommandAapi());
        pm.registerCommand(pl, new CommandLobby());
    }
}
