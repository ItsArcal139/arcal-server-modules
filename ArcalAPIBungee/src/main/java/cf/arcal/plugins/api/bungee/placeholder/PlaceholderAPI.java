package cf.arcal.plugins.api.bungee.placeholder;

import java.util.*;

import net.md_5.bungee.api.connection.*;

public class PlaceholderAPI {
    private static List<Expansion> expansions = new ArrayList<>();

    public static Expansion getExpansionByIdentifier(String identifier) {
        Expansion exp = null;
        for (Expansion e : expansions) {
            if (e != null) {
                if (e.getIdentifier().equals(identifier)) {
                    exp = exp == null ? e : exp;
                }
            }
        }
        return exp;
    }

    public static String parse(ProxiedPlayer p, String str) {
        if (str == null) return "";

        boolean isPlaceholderPart = false;
        String placeholderName = "";
        String placeholderIdentifier = "";
        boolean isPlaceholderName = false;
        boolean escapeNextChar = false;

        HashMap<String, String> pendingReplaces = new HashMap<>();
        for (int i = 0; i < str.length(); i++) {
            String currChar = str.substring(i, i + 1);
            if (currChar.equals("%") && !escapeNextChar) {
                isPlaceholderPart = !isPlaceholderPart;
                isPlaceholderName = false;

                if (!isPlaceholderPart) {
                    // Found a placeholder. Gotta replace them.
                    String pl = "%" + placeholderIdentifier + "_" + placeholderName + "%";
                    Expansion exp = PlaceholderAPI.getExpansionByIdentifier(placeholderIdentifier);
                    if (exp != null) {
                        if (!pendingReplaces.containsKey(pl)) {
                            pendingReplaces.put(pl, exp.onPlaceholderRequest(p, placeholderName));
                        }
                    }
                }
            } else {
                if (isPlaceholderPart) {
                    if (currChar.equals("_")) {
                        if (!isPlaceholderName) {
                            isPlaceholderName = true;
                        } else {
                            placeholderName += currChar;
                        }
                    } else {
                        if (isPlaceholderName) {
                            placeholderName += currChar;
                        } else {
                            placeholderIdentifier += currChar;
                        }
                    }
                }
            }
            if (escapeNextChar) {
                escapeNextChar = false;
            }
            if (currChar.equals("\\")) {
                escapeNextChar = true;
            }
        }

        String result = str;
        for (String from : pendingReplaces.keySet()) {
            String to = pendingReplaces.get(from);
            if (to != null) {
                result = result.replace(from, to);
            }
        }
        return result;
    }

    public static void register(Expansion ex) {
        expansions.add(ex);
    }

    static {
        PlaceholderAPI.register(new AapiExpansion());
    }
}
