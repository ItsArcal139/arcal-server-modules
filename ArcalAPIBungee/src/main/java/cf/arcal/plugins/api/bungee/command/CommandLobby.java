package cf.arcal.plugins.api.bungee.command;

import cf.arcal.plugins.api.bungee.*;
import cf.arcal.plugins.api.bungee.util.*;
import cf.arcal.plugins.api.util.JSONColor;
import cf.arcal.plugins.api.util.JSONTextBuilder;
import net.md_5.bungee.api.*;
import net.md_5.bungee.api.config.*;
import net.md_5.bungee.api.connection.*;

/**
 * @author Arcal
 */
public class CommandLobby extends ArcalCommand {

    public CommandLobby() {
        super("lobby");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        ArcalAPIBungee api = ArcalAPIBungee.getInstance();

        String group = api.getDefaultLobbyGroup();
        if (args.length >= 1) {
            group = args[1];
        }

        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            ServerGroup g = api.getServerGroup("lobby-" + group);
            if (g != null) {
                ServerInfo server = g.chooseServer();
                api.chat().log(
                    JSONTextBuilder.translatable("Sending%s to the server %s in group %s")
                        .addWith(api.getPlayerComponent(p).setColor(JSONColor.GOLD))
                        .addWith(JSONTextBuilder.text(server.getName()).setColor(JSONColor.GOLD))
                        .addWith(JSONTextBuilder.text(g.getName()).setColor(JSONColor.GOLD))
                );
                api.bungee().sendPlayerToServer(p, server);
            } else {
                api.chat().sendToSender(sender, JSONTextBuilder.text("Cannot find a lobby for you! :(").setColor(JSONColor.RED));
                api.chat().log(
                    JSONTextBuilder.text("Failed to find the lobby group. ")
                        .setColor(JSONColor.RED)
                        .addExtra(
                            JSONTextBuilder.translatable("Queried name: %s")
                                .addWith(JSONTextBuilder.text(group).setColor(JSONColor.GOLD))
                                .setColor(JSONColor.WHITE)
                        )
                );
            }
        } else {
            api.chat().log(JSONTextBuilder.text("This command is in-game only.").setColor(JSONColor.RED));
        }
    }
}
