package cf.arcal.plugins.api.bungee.util;

import cf.arcal.plugins.api.util.*;
import net.md_5.bungee.api.chat.*;
import net.md_5.bungee.api.connection.*;

public class BungeePlayerProvider extends PlayerProvider<ProxiedPlayer> {
    private ProxiedPlayer player;

    public BungeePlayerProvider(ProxiedPlayer p) {
        this.player = p;
    }

    @Override
    public ProxiedPlayer getPlayer() {
        return this.getBungeePlayer();
    }

    @Override
    public void sendMessage(String msg) {
        BaseComponent[] c = TextComponent.fromLegacyText(msg);
        this.player.sendMessage(c);
    }

    public ProxiedPlayer getBungeePlayer() {
        return this.player;
    }
}
