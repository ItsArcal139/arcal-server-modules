package cf.arcal.plugins.api.bungee.util;

import cf.arcal.plugins.api.bungee.*;

import java.util.*;
import java.util.concurrent.TimeUnit;

import cf.arcal.plugins.api.util.JSONColor;
import cf.arcal.plugins.api.util.JSONTextBuilder;
import net.md_5.bungee.*;
import net.md_5.bungee.api.*;
import net.md_5.bungee.api.config.*;
import net.md_5.bungee.api.scheduler.*;

public class ServerGroup {
    public enum Algorithm {
        RANDOM, RANDOMFILL_MOST, RANDOMFILL_LEAST
    }

    private ArcalAPIBungee api = null;
    private List<String> registeredServers = new ArrayList<>();
    private Map<String, ServerPing> onlineState = new HashMap<>();
    private Map<String, ScheduledTask> tasks = new HashMap<>();
    private Algorithm algorithm;

    public ServerGroup(String name, Algorithm algorithm) {
        this.api = ArcalAPIBungee.getInstance();
        this.name = name;
        this.algorithm = algorithm;
    }

    public List<String> getServers() {
        return Collections.unmodifiableList(this.registeredServers);
    }

    public ServerGroup(String name) {
        this(name, Algorithm.RANDOM);
    }

    private String name = null;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void registerServerByRegExp(String regex) {
        if (regex == null) {
            api.chat().log(JSONTextBuilder.text("Found null regex! This group will be empty!").setColor(JSONColor.RED));
            return;
        }
        api.chat().log(
            JSONTextBuilder.translatable("Registering servers to group %s by regex %s")
                .addWith(JSONTextBuilder.text(this.getName()).setColor(JSONColor.GOLD))
                .addWith(JSONTextBuilder.text(regex).setColor(JSONColor.GOLD))
        );
        Map<String, ServerInfo> servers = BungeeCord.getInstance().getServers();
        for (String name : servers.keySet()) {
            if (name.matches(regex)) {
                ServerInfo s = servers.get(name);
                this.registerServer(s);
            }
        }
    }

    public boolean isServerOnline(String server) {
        return this.onlineState.get(server) != null;
    }

    public void registerServer(ServerInfo s) {
        if (s == null) {
            api.chat().log(JSONTextBuilder.text("Found null ServerInfo instance! That server will be ignored!").setColor(JSONColor.RED));
            return;
        }
        api.chat().log(
            JSONTextBuilder.text(" - ")
                .addExtra(
                    JSONTextBuilder.translatable("Registering server %s to group %s")
                        .addWith(JSONTextBuilder.text(s.getName()).setColor(JSONColor.GOLD))
                        .addWith(JSONTextBuilder.text(this.getName()).setColor(JSONColor.GOLD))
                )
                .setColor(JSONColor.WHITE)
        );
        this.tasks.put(s.getName(), BungeeCord.getInstance().getScheduler().schedule(api, () -> {
            s.ping((ServerPing ping, Throwable t) -> {
                this.onlineState.put(s.getName(), t == null ? ping : null);
            });
        }, 0, 5, TimeUnit.SECONDS));
        this.registeredServers.add(s.getName());
    }

    public void unregisterServer(ServerInfo s) {
        if (s == null) {
            api.chat().log(JSONTextBuilder.text("Found null ServerInfo instance! Cannot unregister!").setColor(JSONColor.RED));
            return;
        }
        api.chat().log(
            JSONTextBuilder.translatable("Unregistering server %s from group %s")
                .addWith(JSONTextBuilder.text(s.getName()).setColor(JSONColor.GOLD))
                .addWith(JSONTextBuilder.text(this.getName()).setColor(JSONColor.GOLD))
        );
        ScheduledTask t = this.tasks.remove(s.getName());
        t.cancel();
        this.onlineState.remove(s.getName());
        this.registeredServers.remove(s.getName());
    }

    public boolean containsServer(String name) {
        for (String s : this.registeredServers) {
            if (s.equals(name)) return true;
        }
        return false;
    }

    public void unregisterAllServer() {
        List<String> servers = new ArrayList<>();
        servers.addAll(this.registeredServers);
        for (String name : servers) {
            ServerInfo s = this.getServer(name);
            this.unregisterServer(s);
        }
    }

    public Algorithm getAlgorithm() {
        return this.algorithm;
    }

    public void setAlgorithm(Algorithm a) {
        this.algorithm = a;
    }

    public void setAlgoirthm(String a) {
        try {
            this.algorithm = Algorithm.valueOf(a.toUpperCase());
        } catch (IllegalArgumentException ex) {
            this.algorithm = Algorithm.RANDOM;
        }
    }

    public ServerInfo chooseServer() {
        Map<String, ServerInfo> servers = BungeeCord.getInstance().getServers();
        Map<ServerInfo, ServerPing> onlineServersMap = new HashMap<>();
        for (String key : this.onlineState.keySet()) {
            if (this.isServerOnline(key)) {
                // api.chat().log("Server &6"+key+" &fis online, adding to online list...");
                onlineServersMap.put(this.getServer(key), this.onlineState.get(key));
            }
        }

        List<ServerInfo> availableServers = new ArrayList<>();
        for (ServerInfo s : onlineServersMap.keySet()) {
            // api.chat().log("Check if server &6"+s.getName()+" &fis full...");
            ServerPing ping = onlineServersMap.get(s);
            if (ping.getPlayers().getMax() > ping.getPlayers().getOnline()) {
                availableServers.add(s);
                // api.chat().log("Server &6"+s.getName()+" &fis not full, marked as available...");
            }
        }
        if (availableServers.isEmpty()) {
            return null;
        }

        availableServers.sort((ServerInfo a, ServerInfo b) -> {
            switch (this.getAlgorithm()) {
                case RANDOM:
                    return (int) Math.round(Math.random() * 2) - 1;
                case RANDOMFILL_MOST:
                    return a.getPlayers().size() - b.getPlayers().size();
                case RANDOMFILL_LEAST:
                    return b.getPlayers().size() - a.getPlayers().size();
            }
            return 0;
        });

        ServerInfo result = availableServers.get(0);
        if (result != null) {
            api.chat().log(
                JSONTextBuilder.translatable("The server %s is chosen by using the %s algorithm.")
                    .addWith(JSONTextBuilder.text(result.getName()).setColor(JSONColor.GOLD))
                    .addWith(JSONTextBuilder.text(this.getAlgorithm().name().toLowerCase()).setColor(JSONColor.GOLD))
                    .alternativeColorCode('&').setColor(JSONColor.WHITE)
            );
        } else {
            api.chat().log(
                JSONTextBuilder.translatable("Null server is chosen!? Currently using the %s algorithm.")
                    .addWith(JSONTextBuilder.text(this.getAlgorithm().name().toLowerCase()).setColor(JSONColor.GOLD))
                    .alternativeColorCode('&').setColor(JSONColor.WHITE)
            );
        }
        return result;
    }

    private ServerInfo getServer(String name) {
        Map<String, ServerInfo> servers = BungeeCord.getInstance().getServers();
        /*
        if(servers.keySet().contains(name)) {
            return servers.get(name);
        }
        */

        for (String s : servers.keySet()) {
            if (s.equals(name)) return servers.get(s);
        }
        return null;
    }
}
