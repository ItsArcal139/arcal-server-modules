package cf.arcal.plugins.api.bungee.util;

public class TextUtility {
    public static int getServerListSpacePad(String text, int maxSize) {
        int size = 0;
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (c == ' ' || c >= 'A' && c <= 'z' && c != '[' && c != ']' && c != '`' || c == '`' || c < 'A') {
                size += 4;
            } else if (c == '(' || c == ')' || c == '[' || c == ']' || c == '{' || c == '}' || c == '!' || c == '|' || c == '\'' || c == ':' || c == ';' || c == ',' || c == '.') {
                size += 1;
            } else {
                size += 8;
            }
        }
        return (maxSize - size) / 4;
    }
}
