package cf.arcal.plugins.api.bungee.util;

import cf.arcal.plugins.api.util.*;
import net.md_5.bungee.*;
import net.md_5.bungee.api.*;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.connection.*;

public class BungeeChatUtility extends ChatUtility {
    public void sendToSender(CommandSender sender, JSONComponent component) {
        sender.sendMessage(this.processComponent(component));
    }

    private BaseComponent processComponent(JSONComponent component) {
        JSONComponent result = this.getPrefix().clone().addExtra(component);
        return BungeeJSONTextBuilder.build(result)[0];
    }

    @Override
    public void sendToPlayer(PlayerProvider provider, JSONComponent component) {
        ((ProxiedPlayer) provider.getPlayer()).sendMessage(this.processComponent(component));
    }

    public void sendToPlayer(ProxiedPlayer p, JSONComponent component) {
        this.sendToPlayer(new BungeePlayerProvider(p), component);
    }

    @Override
    public void broadcast(JSONComponent component) {
        for (ProxiedPlayer p : BungeeCord.getInstance().getPlayers()) {
            this.sendToPlayer(p, component);
        }
    }

    @Override
    public void log(JSONComponent component) {
        BungeeCord.getInstance().getConsole().sendMessage(this.processComponent(component));
    }
}
